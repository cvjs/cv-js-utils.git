# 加载

### 方法概览

| 方法                                    | 返回   | 说明                     |
| :-------------------------------------- | :----- | :----------------------- |
| [loadCssPromise](#loadcsspromise)       | Promise | Promise动态加载css |
| [loadScript](#loadscript)               | - | 异步加载js         |
| [loadScriptSync](#loadscriptsync)       | - | 同步加载js         |
| [loadScriptPromise](#loadscriptpromise) | Promise | Promise动态加载js  |
| [loadIframePromise](#loadiframepromise) | Promise | 动态加载iframe     |
