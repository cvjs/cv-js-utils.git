# 拷贝

### 方法概览

| 方法                          | 返回   | 说明                     |
| :---------------------------- | :----- | :----------------------- |
| [objDeepClone](#objdeepclone) | Array | 数组深度克隆         |
| [objDeepCopy](#objdeepcopy)   | Array | 数组深度拷贝         |
| [cloneData](#clonedata)       | * | 克隆对象           |
| [cloneDeep](#clonedeep)       | Mixid | 适用于任何数组和对象的深拷贝 |
