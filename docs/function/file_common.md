# 文件操作

### 方法概览

| 方法                                | 返回   | 说明                     |
| :---------------------------------- | :----- | :----------------------- |
| [fileGetExt](#filegetext)           | String | 获取文件后缀   |
| [fileCheckImg](#filecheckimg)       | Boolean | 验证文件是否图片 |
| [fileImgCompress](#fileimgcompress) | Promise | 压缩图片     |
| [fileBytesToSize](#filebytestosize) | String | 字节转换     |
