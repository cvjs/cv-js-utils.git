# object对象

### 方法概览

| 方法                          | 返回   | 说明                     |
| :---------------------------- | :----- | :----------------------- |
| [objectSort](#objectsort)     | Object | 排序object(JSON)对象 |
| [objectEquals](#objectequals) | Boolean | 判断两个对象是否相等       |
