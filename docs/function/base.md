# 基础

### 方法概览

| 方法                          | 返回   | 说明                     |
| :---------------------------- | :----- | :----------------------- |
| [baseThrottle](#basethrottle) | - | 函数节流       |
| [baseDebounce](#basedebounce) | - | 函数防抖       |
| [parseNumber](#parsenumber)   | Number | 相当于 intval |
