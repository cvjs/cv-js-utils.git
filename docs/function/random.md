# 生成随机

### 方法概览

| 方法                          | 返回   | 说明                     |
| :---------------------------- | :----- | :----------------------- |
| [randomUUID](#randomuuid)     | String | 生成随机UUID           |
| [randomString](#randomstring) | String | 生成随机字符串 (大写、小写、数字) |
| [randomWord](#randomword)     | String | 生成随机字符串 (大写、小写、数字) |
| [randomNum](#randomnum)       | Number | 生成随机范围数字           |
