# 文件转换

### 方法概览

| 方法                                    | 返回   | 说明                     |
| :-------------------------------------- | :----- | :----------------------- |
| [dataURL_to_blob](#dataurl_to_blob)     | - | dataUrl to Blob         |
| [blob_to_dataURL](#blob_to_dataurl)     | - | blob to dataURL         |
| [dataURL_to_file](#dataurl_to_file)     | File | 将base64/dataurl转成File   |
| [base64_to_blob](#base64_to_blob)       | - | 将base64转成blob           |
| [base64_to_blobURL](#base64_to_bloburl) | - | base 64 转 blob url      |
| [getObjectURL](#getobjecturl)           | - | 获取File 对象或 Blob 对象的临时路径 |
