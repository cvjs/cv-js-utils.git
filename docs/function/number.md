# 数字

### 方法概览

| 方法                          | 返回   | 说明                     |
| :---------------------------- | :----- | :----------------------- |
| [numberAccAdd](#numberaccadd) | Number | 加法          |
| [numberAccSub](#numberaccsub) | Number | 减法          |
| [numberAccMul](#numberaccmul) | Number | 乘法          |
| [numberAccDiv](#numberaccdiv) | Number | 除法          |
| [numberRound](#numberround)   | Number | 四舍五入（保留小数点） |
