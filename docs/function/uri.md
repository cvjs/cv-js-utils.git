# uri

### 方法概览

| 方法                                      | 返回   | 说明                     |
| :---------------------------------------- | :----- | :----------------------- |
| [urlGetDomain](#urlgetdomain)             | String | 正则提取域名       |
| [urlRemoveParameter](#urlremoveparameter) | String | 删除地址中的参数     |
| [urlAddParams](#urladdparams)             | Mixed | 连接加上参数       |
| [urlQueryToObj](#urlquerytoobj)           | Object | 获取uri参数      |
| [urlObjToQuery](#urlobjtoquery)           | String | 对象转成url查询字符串 |
