# 字符串处理

### 方法概览

| 方法                                | 返回   | 说明                     |
| :---------------------------------- | :----- | :----------------------- |
| [strReplace](#strreplace)           | String | 全局替换某个字符为另一个字符          |
| [strFormatMoney](#strformatmoney)   | Number | 金额千位分隔符                 |
| [strFourSeparate](#strfourseparate) | - | 字符每4位数一个间隔              |
| [strHideBankNo](#strhidebankno)     | String | 隐藏银行卡号（ 仅显示后四位,其他*代替 ）  |
| [strHideMobile](#strhidemobile)     | String | 隐藏手机号（ 仅显示前3后4位,其他*代替 ） |
| [strSplitChars](#strsplitchars)     | String | 将字符串拆成字符，并存到数组中         |
| [strFilterCN](#strfiltercn)         | String | 过滤某字符串中的中文字符            |
| [strSubCHString](#strsubchstring)   | String | 截取字符串（从start字节到end字节）   |
| [strSubString](#strsubstring)       | String | 截取字符串                   |
| [strGetMiddle](#strgetmiddle)       | Mixed | 取字符串中间                  |
