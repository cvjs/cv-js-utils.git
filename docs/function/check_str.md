# 检测字符串

### 方法概览

| 方法                        | 返回   | 说明                     |
| :-------------------------- | :----- | :----------------------- |
| [isStrEN](#isstren)         | Boolean | 是否 英文     |
| [isStrZh](#isstrzh)         | Boolean | 是否 全中文    |
| [isStrZhOrEn](#isstrzhoren) | Boolean | 是否 中文+英文  |
| [isStrCHS](#isstrchs)       | Boolean | 某个字符是否是汉字 |
| [isStrAbroad](#isstrabroad) | Boolean | 只有中文和英文   |
