# array递归树形

### 方法概览

| 方法                                      | 返回   | 说明                     |
| :---------------------------------------- | :----- | :----------------------- |
| [arrayToTree](#arraytotree)               | Array | 将数组转化成树结构       |
| [treeToSubFlatten](#treetosubflatten)     | Array | 树形数据 向下 递归为一维数组 |
| [treeToSupFlatten](#treetosupflatten)     | Array | 树形数据 向上 递归为一维数组 |
| [treeCalcLevel](#treecalclevel)           | Number | 递归计算树形数据最大的层级数  |
| [treeDeepInclude](#treedeepinclude)       | Array | 树形数据递归筛选目标值     |
| [treeRegDeepParents](#treeregdeepparents) | Array | 根据条件递归祖先元素      |
