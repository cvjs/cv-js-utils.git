# 颜色

### 方法概览

| 方法                                    | 返回   | 说明                     |
| :-------------------------------------- | :----- | :----------------------- |
| [colorRgbaToHexify](#colorrgbatohexify) | String | rgba转16进制的函数 |
| [colorRandomRgba](#colorrandomrgba)     | String | 随机生成rgb颜色    |
| [colorToImgRbg](#colortoimgrbg)         | String | 根据图片生成一些相近颜色 |
