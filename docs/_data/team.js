export const core = [
  {
    avatar: 'https://www.10yun.com/logo-icon.png',
    name: 'shiyun',
    title: 'cvjs',
    org: 'shiyun',
    orgLink: 'https://www.10yun.com/',
    desc: '十云科技',
    links: [
      { icon: 'github', link: 'https://github.com/10yun' },
      {
        icon: {
          svg: '<img src="https://gitee.com/assets/favicon.ico" style="width: 20px;height: 20px;" />'
        },
        link: 'https://gitee.com/sykj'
      }
    ],
    sponsor: 'https://gitee.com/sykj'
  },
  {
    avatar: 'https://www.10yun.com/logo-icon.png',
    name: 'trystan',
    title: '开发者',
    org: 'shiyun',
    orgLink: 'https://www.10yun.com/',
    desc: '十云科技',
    links: [
      { icon: 'github', link: 'https://github.com/10yun' },
      {
        icon: {
          svg: '<img src="https://gitee.com/assets/favicon.ico" style="width: 20px;height: 20px;" />'
        },
        link: 'https://gitee.com/sykj'
      }
    ],
    sponsor: 'https://gitee.com/sykj'
  },
  {
    avatar: 'https://www.10yun.com/logo-icon.png',
    name: 'ctocode',
    title: '开发者',
    org: 'shiyun',
    orgLink: 'https://www.10yun.com/',
    desc: '十云科技',
    links: [
      { icon: 'github', link: 'https://github.com/10yun' },
      {
        icon: {
          svg: '<img src="https://gitee.com/assets/favicon.ico" style="width: 20px;height: 20px;" />'
        },
        link: 'https://gitee.com/sykj'
      }
    ],
    sponsor: 'https://gitee.com/sykj'
  },
  {
    avatar: 'https://www.10yun.com/logo-icon.png',
    name: 'ysh',
    title: '开发者',
    org: 'shiyun',
    orgLink: 'https://www.10yun.com/',
    desc: '十云科技',
    links: [
      // https://github.com/fluidicon.png
      { icon: 'github', link: 'https://github.com/10yun' },
      {
        icon: {
          svg: '<img src="https://gitee.com/assets/favicon.ico" style="width: 20px;height: 20px;" />'
        },
        link: 'https://gitee.com/sykj'
      }
    ],
    sponsor: 'https://gitee.com/sykj'
  }
];

export const emeriti = [
  // {
  //   avatar: 'https://github.com/underfin.png',
  //   name: 'underfin',
  //   title: '开发者',
  //   links: [{ icon: 'github', link: 'https://github.com/underfin' }]
  // },
  // {
  //   avatar: 'https://github.com/GrygrFlzr.png',
  //   name: 'GrygrFlzr',
  //   title: '开发者',
  //   links: [{ icon: 'github', link: 'https://github.com/GrygrFlzr' }]
  // },
  // {
  //   avatar: 'https://github.com/nihalgonsalves.png',
  //   name: 'Nihal Gonsalves',
  //   title: '高级软件工程师',
  //   links: [{ icon: 'github', link: 'https://github.com/nihalgonsalves' }]
  // }
];

export const cnTranslator = [
  // {
  //   avatar: 'https://www.github.com/ShenQingchuan.png',
  //   name: 'ShenQingchuan',
  //   title: '前端开发者',
  //   org: 'Bytedance',
  //   orgLink: 'https://www.bytedance.com/',
  //   desc: 'cvjs 团队中文翻译维护者',
  //   links: [
  //     { icon: 'github', link: 'https://github.com/ShenQingchuan' },
  //     { icon: 'twitter', link: 'https://twitter.com/ShenQingchuan' }
  //   ]
  // }
];
