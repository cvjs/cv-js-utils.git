import { defineConfig } from 'vitepress';
const ogTitle = 'cvjs';
const ogKeyword = 'cvjs,js工具库,函数';
const ogDescription = 'cvjs常用工具类函数';
const ogImage = 'https://vitejs.dev/og-image.png';
const ogUrl = 'https://cvjs.cn';

import renderPermaLink from './render-perma-link';
import MarkDownItCustomAnchor from './markdown-it-custom-anchor';

// import configConfig from './config/config_config.js';

import guideAll from '../guide/guide.js';
import classAll from '../class/class.js';
import functionAll from '../function/_menu.js';
import documentAll from '../document/document.js';
// export default {
export default defineConfig({
  // 网站标题
  title: 'js 常用工具类函数',
  author: '十云',
  //网站描述
  description: 'cvjs.cn、十云、10yun、shiyun',
  //  部署时的路径 默认 /  可以使用二级地址 /base/
  base: '/cv-js-utils/',
  outDir: '../dist',
  //语言
  // lang: 'zh-CN',
  // lang: 'cn-ZH',
  // lang: 'en-US',
  // vue: {
  //   reactivityTransform: true
  // },
  lastUpdated: true,
  // 改变title的图标
  head: [
    //图片放在public文件夹下
    ['link', { rel: 'icon', type: 'image/svg+xml', href: '/logo.svg' }],
    ['link', { rel: 'icon', href: '/img/linktolink.png' }],
    ['meta', { property: 'og:type', content: 'website' }],
    ['meta', { property: 'og:title', content: ogTitle }],
    ['meta', { property: 'og:image', content: ogImage }],
    ['meta', { property: 'og:url', content: ogUrl }],
    ['meta', { property: 'og:description', content: ogDescription }],
    ['meta', { name: 'twitter:card', content: 'summary_large_image' }],
    ['meta', { name: 'twitter:site', content: '@vite_js' }],
    ['meta', { name: 'theme-color', content: '#646cff' }],
    ['meta', { name: 'keywords', content: ogKeyword }],
    ['meta', { name: 'description', content: ogDescription }]
    // ['style', {}, "a[title='站长统计'] {display: none;}"],
    // ['script ', { type: 'text/javascript', src: '' }]

    // [
    //   'script',
    //   {
    //     src: 'https://cdn.usefathom.com/script.js',
    //     'data-site': 'CBDFBSLI',
    //     'data-spa': 'auto',
    //     defer: ''
    //   }
    // ],
  ],
  // 主题配置
  themeConfig: {
    logo: '/logo-nobg.png',
    outline: 3, //需要的大纲级别
    outlineTitle: '内容大纲',
    lastUpdatedText: '最近更新时间',
    editLink: {
      editLinks: true,
      text: '为此页在<码云>上提供修改建议',
      // pattern: 'https://github.com/10yun/cv-mobile-docs/edit/main/:path',
      // pattern: 'https://gitee.com/cvjs/cv-mobile-docs/blob/master/docs/:path',
      pattern: 'https://gitee.com/cvjs/cv-mobile-ui/issues/new?title=文档建议:path'
    },
    docFooter: {
      prev: '上一篇',
      next: '下一篇'
    },
    footer: {
      message: 'Copyright © 2017 10yun.com | 十云提供计算服务-IPV6 | ctocode组开发',
      copyright:
        '<a href="https://beian.miit.gov.cn" target="_blank">闽ICP备18000673号-5</a><br/>本文档内容版权为 cvjs 官方团队所有，保留所有权利。'
    },
    socialLinks: [
      {
        icon: {
          svg: '<img src="https://gitee.com/assets/favicon.ico" style="width: 20px;height: 20px;" />'
        },
        link: 'https://gitee.com/cvjs/cv-js-utils'
      },
      { icon: 'github', link: 'https://github.com/10yun' },
      { icon: 'github', link: 'https://github.com/vuejs/vitepress' }
    ],
    // algolia: {
    //   appId: '7H67QR5P0A',
    //   apiKey: 'deaab78bcdfe96b599497d25acc6460e',
    //   indexName: 'vitejs',
    //   searchParameters: {
    //     facetFilters: ['tags:cn']
    //   }
    // },

    // carbonAds: {
    //   code: 'CEBIEK3N',
    //   placement: 'vitejsdev'
    // },
    localeLinks: {
      text: '简体中文',
      items: [
        // { text: 'English', link: 'https://vitejs.dev' },
        // { text: '日本語', link: 'https://ja.vitejs.dev' },
        // { text: 'Español', link: 'https://es.vitejs.dev' }
      ]
    },
    // 头部导航
    nav: [
      { text: '指南', link: '/guide/', activeMatch: '/guide/' },
      { text: '类库', link: '/class/', activeMatch: '/class/' },
      { text: '函数', link: '/function/', activeMatch: '/function/' },
      { text: 'document', link: '/document/', activeMatch: '/document/' },
      { text: '❤ 赞助我们', link: '/guide/sponsor', activeMatch: '/guide/' }
    ],
    // 侧边导航
    sidebar: {
      '/guide/': guideAll,
      '/class/': classAll,
      '/function/': functionAll,
      '/document/': documentAll
    }
  },
  markdown: {
    // 显示行号
    // lineNumbers: true,
    anchor: {
      permalink: renderPermaLink
    },
    config: (md) => {
      md.use(MarkDownItCustomAnchor);
    }
  }
});
