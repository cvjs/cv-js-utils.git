# 常用类库 

| 类名         | 作用     |
| :----------- | :------- |
| RequestClass | 网络请求 |
| MessageClass | 消息     |
| JumpsClass   | 跳转     |
| LbsClass     | 位置服务 |
| StorageClass | 缓存     |
