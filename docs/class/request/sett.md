# 请求配置

### 需要下载依赖

```sh

npm install axios
# 或者
pnpm install axios
```


### 接口flag配置


**1、自定义接口flag**  

创建接口配置文件 ` 项目/src/api/index.js ` 

```js
export default {
  API_FLAG111: '/v1/login/xxxx',
  API_FLAG222: '/v1/goods'
  ...
};
```

**2、[flag:ur]参考对应的业务接口文档**  


### 局部调用

添加扩展 ` 项目/src/plugins/http.js ` 

```js  

// 加载插件
import RequestClass from '@10yun/cv-js-utils/class/request.js';
// 加载 错误提示
import { ElNotification } from 'element-plus';
// 加载接口flag配置
import ApiFlagAll from '@/api/index.js';

import StorageClass from './storage';
// import store from "store";

// 有些flag配置是请求后获取到的，需要重新获取 flag
function getFlagAll() {
  let ApiFlagAll = {};
  // 可以获取flag缓存
  ApiFlagAll = Object.assign(ApiFlagAll, ApiFlagSet || {});
  // console.log(ApiFlagAll);
  return ApiFlagAll;
}

/**
 * 【必须】 ，自定义输出错误
 */
RequestClass.prototype.resetNetError = function (error) {
  // vue3
  let message = (error.msg || error.message) + (error.status ? `(${error.status})` : '');
  ElNotification({
    title: '提示',
    message: message,
    type: error.type || 'info',
    duration: 1200,
    onClose: () => {
      // 关闭时候，有其他的操作??
      // return Promise.reject(error);
    }
  });
};

/** 自定义错误 响应 */
RequestClass.prototype.setResponse = function (apiResData) {
  var apiResStatus = apiResData.status || -1;
  apiResStatus = parseInt(apiResData.status);
  // 这里根据后端提供的数据进行对应的处理
  switch (apiResStatus) {
    case 200:
      break;
    case 404:
      return Promise.reject(apiResData);
      break;
  }
};

/**
 * 如果要请求本地的url，可以这样
 */
RequestClass.prototype.getLocal = function (url, options) {
  options = options || {};
  return this.urlGet('http://localhost/' + url, options);
};



// 示例对象
var HttpObj = new RequestClass({
  // vue3
  baseURL: process.env.VITE_API_URL || '',
  // vue2
  baseURL: process.env.VUE_APP_API_URL || '',
  // 或者

  baseURL: 'https://api.xxx.com/',
  flagFunc: getFlagAll,
  flagMap: getFlagAll(),
  flagMap: ApiFlagAll, //接口配置参数
  // storeHandle: store,//store句柄
  // storeHandle: $store, //store句柄
  needMethods: ['PATCH'],
  headers: {
    Token: Storage('token') || '',
    ClientPlatform: 'pc',
    Authorization: Storage('token_jwt') || ''
  },
  requests: {}
});

export default HttpObj;









```

在用到的地方调用

```js
import HttpObj from "@/plugins/http.js";


HttpObj.flagGet('XX_XXX_XXX', {
  pagesize: 12,
  page: currPage
}).then(() => {
  // ok 时候
})
.catch((error) => {
  // 错误时候
});


```


### 全局调用

如果需要绑定全局

添加全局 ` 项目/src/plugins/bindGlobal/http.js ` 

```js

// vue3
import HttpObj from "@/plugins/http.js";
app.config.globalProperties.$request = HttpObj;

```

在用到的地方调用
```js

this.$request.方法(参数);

```



### Nuxt3 中调用

```js

// 把导入的
import RequestClass from '@10yun/cv-js-utils/class/request.js';
// 改成
import RequestClass from '@10yun/cv-js-utils/class/request-nuxt.js';


```