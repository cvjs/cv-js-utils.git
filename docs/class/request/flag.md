# flag 请求方式（推荐）

### 简要

- flag方式请求，可以在接口url变更后，只需修改flag配置，无需改动业务代码

### 参数 

- 当参数2个时

| 参数      | 类型   | 是否必需 | 说明                  |
| :-------- | :----- | :------- | :-------------------- |
| api_flag  | string | 是       | api接口标识           |
| api_param | object | 是       | api发送参数，默认空{} |

方法如下：
```js
// 模式1
flagXxx(api_flag,api_param).then((succRes)=>{   });
// 模式2，一般不用
flagXxx(api_flag)(api_param).then((succRes)=>{   });
```


- 当参数3个时
  
| 参数      | 类型   | 是否必需 | 说明                         |
| :-------- | :----- | :------- | :--------------------------- |
| api_flag  | string | 是       | api接口标识                  |
| api_id    | int    | 否       | flag转url拼接成  /xxx/xxx/id |
| api_param | object | 是       | api发送参数，默认空{}        |

方法如下：
```js
// 模式1
flagXxx(api_flag,api_id,api_param).then((succRes)=>{   });
// 模式2，一般不用
flagXxx(api_flag,api_id)(api_param).then((succRes)=>{   });
```

### flagGet
  
```js
import HttpObj from "@/plugins/http.js";

HttpObj.flagGet('FLAG参考API手册',{
    key1 : val1,
    key2 : val3,
    ...更多key : ...更多val
}).then((succRes)=>{

});
```

### flagPost
  
```js
import HttpObj from "@/plugins/http.js";

HttpObj.flagPost('FLAG参考API手册',{
    key1:val1,
    key2:val3,
    ...更多key : ...更多val
}).then((succRes)=>{

});
```


### flagPut

如 flagGet、flagPost调用


### flagPatch

如 flagGet、flagPost调用


### flagDel

如 flagGet、flagPost调用