

## 设置缓存 setCacheData

****

**流程**

- 1、发起接口请求  
- 2、请求成功，缓存数据，缓存标识为 `XXXXXX_ID` （自定义）  
- 3、继续 then 

```js
import HttpObj from "@/plugins/http.js";
HttpObj.setCacheData(`XXXXX_{id}`)
  .flagGet('XXXXXX',id).then(()=>{

  });
```

## 验证缓存 checkCacheData
  
，：

**流程**

- 1、读取缓存标识为 `XXXXXX_ID` 的缓存数据   
- 2、判断是否数据有无变化  
  - 2.1 有变化时：继续发送接口请求
  - 2.2 无变化时，不进行接口请求 
- 3、继续 then 


```js
import HttpObj from "@/plugins/http.js";
HttpObj.checkCacheData(`XXXXX_{id}`)
  .flagPut('XXXXXX',id).then(()=>{

  });
```
