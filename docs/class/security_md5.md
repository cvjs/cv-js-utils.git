## 安全加密 md5

依赖其他
```
cnpm install crypto-js --save-dev
cnpm install jsencrypt --save-dev
cnpm install js-md5 --save-dev
```



## md5 方法

| 方法         | 返回   | 说明    | 参数     |
| :----------- | :----- | :------ | :------- |
| secMd5Encode | String | md5加密 | (String) |

