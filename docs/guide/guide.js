export default [
  {
    text: '开发指南',
    collapsed: false,
    items: [
      { text: '介绍', link: '/guide/index' },
      { text: '快捷使用', link: '/guide/quickstart' },
      { text: '更新日志', link: '/guide/changelog' },
      { text: '问答', link: '/guide/questions' },
      { text: '赞助我们', link: '/guide/sponsor' }
    ]
  }
];
