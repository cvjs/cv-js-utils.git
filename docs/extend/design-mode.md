
### 设计模式
#### 观察者模式（发布-订阅模式）
```javascript
// 订阅消息
Utils.Observer.subscribe('test', function (e) {
  console.log(e);
});
// 发布消息
Utils.Observer.publish('test', {
  msg: '参数'
});
```

  