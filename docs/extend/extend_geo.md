## 常用字典


### 国家 Country

- Country 模块
- country未内置在Utils模块里，需要单独引入
- 全世界国家和地区的英文名、中文名、英文简称、国家区号数组

```js
import DictGeoCountry from '@10yun/cv-js-utils/dict/geo_country'
console.log(DictGeoCountry);
```

### 省 

### 市

### 区