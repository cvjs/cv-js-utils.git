export default [
  {
    text: '扩展',
    items: [
      { text: '银行', link: '/function/extend_bank' },
      { text: '字典geo', link: '/function/extend_geo' }
    ]
  },
  {
    text: '文件',
    items: [
      { text: '文件常用', link: '/function/file_common' },
      { text: '文件图片', link: '/function/file_image' }
    ]
  }
];
