/**
 * =============================================================================
 * *****************************   sessionStorage   ****************************
 * =============================================================================
 */
/*
 * 设置sessionStorage
 */
export const setSessionStorage = (name, val) => {
  sessionStorage.setItem(name, JSON.stringify(val));
};

/*
 * 通过参数获取sessionStorage的数据
 */
export const getSessionStorage = (name) => {
  if (sessionStorage.hasOwnProperty(name)) {
    return JSON.parse(sessionStorage.getItem(name));
  } else {
    return false;
  }
};

/*
 * 附加sessionStorage, type==true 则是整个修改，
 */
export const addSessionStorage = (name, val) => {
  if (sessionStorage.hasOwnProperty(name)) {
    let old = JSON.parse(sessionStorage.getItem(name));
    let obj = Object.assign(old, val);
    sessionStorage.setItem(name, JSON.stringify(obj));
  } else {
    sessionStorage.setItem(name, JSON.stringify(val));
  }
};

/*
 * 删除sessionStorage
 */
export const deleteSessionStorage = (name = null) => {
  if (name != null) {
    sessionStorage.removeItem(name);
  } else {
    sessionStorage.clear();
  }
};

class StorageSession {
  constructor(options) {
    /**
     * 缓存的key
     */
    this.keyExpired = {};
    this.keyGroup = [];

    this.__open_log = false;
    this.__timer = {};
  }
  setSessionStorage(key, value) {
    return this.__operationSessionStorage(key, value);
  }

  getSessionStorageValue(key) {
    return this.__operationSessionStorage(key);
  }

  sessGetString(key, def = '') {
    let value = this.__operationSessionStorage(key);
    return typeof value === 'string' || typeof value === 'number' ? value : def;
  }

  getSessionStorageInt(key, def = 0) {
    let value = this.__operationSessionStorage(key);
    return typeof value === 'number' ? value : def;
  }

  __operationSessionStorage(key, value) {
    if (!key) {
      return;
    }
    let keyName = '__state__';
    if (key.substring(0, 5) === 'cache') {
      keyName = '__state:' + key + '__';
    }
    if (typeof value === 'undefined') {
      return this.__loadFromlSession(key, '', keyName);
    } else {
      this.__savaToSession(key, value, keyName);
    }
  }

  __savaToSession(key, value, keyName) {
    try {
      if (typeof keyName === 'undefined') keyName = '__seller__';
      let seller = window.sessionStorage.getItem(keyName);
      if (!seller) {
        seller = {};
      } else {
        seller = JSON.parse(seller);
      }
      seller[key] = value;
      window.sessionStorage.setItem(keyName, JSON.stringify(seller));
    } catch (e) {}
  }

  __loadFromlSession(key, def, keyName) {
    try {
      if (typeof keyName === 'undefined') keyName = '__seller__';
      let seller = window.sessionStorage.getItem(keyName);
      if (!seller) {
        return def;
      }
      seller = JSON.parse(seller);
      if (!seller || typeof seller[key] === 'undefined') {
        return def;
      }
      return seller[key];
    } catch (e) {
      return def;
    }
  }
}

export default StorageSession;
