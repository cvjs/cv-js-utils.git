/**
 * =============================================================================
 * *****************************   localStorage   ******************************
 * =============================================================================
 */
/*
 * 删除localStorage
 */
export const deleteLocalStorage = (name = null) => {
  if (name != null) {
    localStorage.removeItem(name);
  } else {
    localStorage.clear();
  }
};
/*
 * 设置localStorage
 */
export const setLocalStorage = (name, val) => {
  localStorage.setItem(name, JSON.stringify(val));
};

/*
 * 通过参数获取localStorage的数据
 */
export const getLocalStorage = (name) => {
  if (localStorage.hasOwnProperty(name)) {
    return JSON.parse(localStorage.getItem(name));
  } else {
    return false;
  }
};
/*
 * 附加localStorage, type==true 则是整个修改，
 */
export const addLocalStorage = (name, val) => {
  if (localStorage.hasOwnProperty(name)) {
    let old = JSON.parse(localStorage.getItem(name));
    let obj = Object.assign(old, val);
    localStorage.setItem(name, JSON.stringify(obj));
  } else {
    localStorage.setItem(name, JSON.stringify(val));
  }
};
export default {
  setLocal(key, value) {
    return this.__operationStorage(key, value);
  },
  getLocal(key) {
    return this.__operationStorage(key);
  },
  getStorageString(key, def = '') {
    let value = this.__operationStorage(key);
    return typeof value === 'string' || typeof value === 'number' ? value : def;
  },
  getStorageInt(key, def = 0) {
    let value = this.__operationStorage(key);
    return typeof value === 'number' ? value : def;
  },
  getStorageBoolean(key, def = false) {
    let value = this.__operationStorage(key);
    return typeof value === 'boolean' ? value : def;
  },
  getStorageArray(key, def = []) {
    let value = this.__operationStorage(key);
    return this.isArray(value) ? value : def;
  },

  getStorageJson(key, def = {}) {
    let value = this.__operationStorage(key);
    return this.isJson(value) ? value : def;
  },
  /**
   * 新增&&获取缓存数据
   * @param key
   * @param value
   * @returns {*}
   */
  __operationStorage(key, value) {
    if (!key) {
      return;
    }
    let keyName = '__state__';
    if (key.substring(0, 5) === 'cache') {
      keyName = '__state:' + key + '__';
    }
    if (typeof value === 'undefined') {
      return this.__loadFromlLocal(key, '', keyName);
    } else {
      this.__savaToLocal(key, value, keyName);
    }
  },
  /**
   *  新增&&修改本地缓存
   *  @param {string} id 唯一id
   *  @param {string} key 标示
   *  @param value 新增&修改的值
   *  @param keyName 主键名称
   */
  __savaToLocal(key, value, keyName) {
    try {
      if (typeof keyName === 'undefined') keyName = '__seller__';
      let seller = window.localStorage[keyName];
      if (!seller) {
        seller = {};
      } else {
        seller = JSON.parse(seller);
      }
      seller[key] = value;
      window.localStorage[keyName] = JSON.stringify(seller);
    } catch (e) {}
  },
  /**
   *  查询本地缓存
   *  @param {string} id 唯一id
   *  @param {string} key 标示
   *  @param def 如果查询不到显示的值
   *  @param keyName 主键名称
   */
  __loadFromlLocal(key, def, keyName) {
    try {
      if (typeof keyName === 'undefined') keyName = '__seller__';
      let seller = window.localStorage[keyName];
      if (!seller) {
        return def;
      }
      seller = JSON.parse(seller);
      if (!seller || typeof seller[key] === 'undefined') {
        return def;
      }
      return seller[key];
    } catch (e) {
      return def;
    }
  }
};
