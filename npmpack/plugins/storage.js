/**
 * 参数存储,缓存
 * @type {{set(*, *): void, get(*): *, del(*): void, clear(): void}}
 */
class StorageClass {
  constructor(options) {
    /**
     * 缓存的key
     */
    this.keyExpired = {};
    this.keyGroup = [];
    /**
     * 缓存时间
     */
    let defaultExpired = 60 * 60 * 24 * 7; // 默认7天
    this.expired = options.expired || defaultExpired;
  }
  _encodeData() {
    // var tempParamObj = JSON.parse(window.localStorage.getItem('cache_api_store'));
    // tempParamObj[key] = value;
    // window.localStorage.setItem('cache_api_store', JSON.stringify(tempParamObj));
  }
  _decodeData() {}
  _deleteKey(key) {
    if (key) {
      delete this.keyExpired[key];
      this.keyGroup = this.keyGroup.filter((item) => item !== key);
    } else {
      this.keyExpired = {};
      this.keyGroup = [];
    }
  }
  setCache(key, val) {
    window.localStorage[key] = typeof val == 'string' ? val : JSON.stringify(val);
  }
  /**
   * -------------------------------------------------------会话存储------------------------------------
   * 会话存储-存
   * @param {*} key 键名
   * @param {*} value 值
   */
  sessionSet(key, oldData, expired) {
    let newData = JSON.stringify(oldData);
    // if (history.hasOwnProperty(key) && key !== '::count') {
    // let history = JSON.parse(window.sessionStorage['__history__'] || '{}');
    sessionStorage.setItem(key, newData);

    this.keyExpired[key] = expired || this.expired;
    this.keyGroup.push(key);
  }

  /**
   * 会话存储-取
   * @param {*} key 键名
   */
  sessionGet(key) {
    let _data = sessionStorage.getItem(key);
    try {
      return JSON.parse(_data);
    } catch (e) {
      return _data;
    }
  }

  /**
   * 会话存储-删
   * @param {*} key 键名
   */
  sessionDel(key) {
    if (!window.sessionStorage) return false;
    sessionStorage.removeItem(key);
    this._deleteKey(key);
  }

  /**
   * 会话存储-删处全部
   */
  sessionClear() {
    if (!window.sessionStorage) return false;
    sessionStorage.clear();
    for (let i in this.keyGroup) {
      sessionStorage.removeItem(i);
    }
    this._deleteKey();
  }

  /**
   * -------------------------------------------------------永久存储-----------------------------------
   * 永久存储-存
   * @param {*} key 键名
   * @param {*} value 值
   */
  /**
   * @description localStorage set
   * @param key 只支持 字符串和数字
   * @param val 支持除了Function和Symbol的所有数据类型
   */
  localSet(key, value) {
    if (!window.localStorage) {
      console.warn('浏览器不支持localStorage');
      return;
    }
    // if (value === '' || value === null || value === undefined) {
    //   value = null;
    // }
    window.localStorage.setItem(key, JSON.stringify(value));
  }
  /**
   * 永久存储-取
   * @param {*} key 键名
   */
  localGet(key) {
    if (!window.localStorage) {
      console.warn('浏览器不支持localStorage');
      return;
    }
    let _data = window.localStorage.getItem(key);
    try {
      if (Object.keys(_data).length == 0 || JSON.stringify(_data) == '{}') {
        _data = '';
      } else {
        // 取缓存,如果解析出来的是对象, 则返回对象
        _data = JSON.parse(_data);
      }
    } catch (e) {
      // console.warn("val不是对象或者数组");
    }
    return _data;
  }
  /**
   * 永久存储-删
   * @description 移除某一个key的storage
   * @param {*} key 键名
   */
  localDel(key) {
    if (!window.localStorage) return false;
    window.localStorage.removeItem(key);
    this._deleteKey(key);
  }
  // 清空
  localClear() {
    if (!window.localStorage) return false;
    window.localStorage.clear();
    for (let i in this.keyGroup) {
      window.localStorage.removeItem(i);
    }
    this._deleteKey();
  }
}
export default StorageClass;
