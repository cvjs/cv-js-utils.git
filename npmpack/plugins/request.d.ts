// import type { AxiosResponse } from 'axios';
interface TypeClassOptions {
  baseURL?: string;
  storeHandle?: any;
  flagMap?: object;
  requests?: object;
  headers?: object;
  timeout?: number;
  retries?: number;
  encryptURL?: array; // 需要加密的url
}

interface TypeOption {
  // onDownloadProgress?: (progressEvent: AxiosProgressEvent) => void;
  // signal?: GenericAbortSignal;
}
interface TypeNetResponse<T = any> {
  data: T;
  message: string | null;
  status: number;
}

interface TypeNetError {
  message?: string;
  msg?: string;
  type?: string;
}

// 指定 api 后端返回的基本数据类型
interface TypeNetApiData {
  code: number;
  status: number;
  data: any;
  msg: string;
}

declare class RequestClass {
  storeHandle: any;
  baseURL: string;
  flagMap: object;
  // 参数
  requests: object;
  headers: object;
  canshuOpt: object;
  //
  creSett: object;
  reqObj: any;

  // 构造函数
  constructor(options: TypeClassOptions);
  /**
   * 设置 API
   */
  setApiFlag(options: object);
  appendApiFlag(options: object);
  setApiAuth(options: object);
  appendApiAuth(options: object);

  // 设置头部参数
  setDefHeaders(options: object);
  setHeaders(options: object);
  // 设置 request
  setDefRequests(options: object);
  setRequests(options: object);
  // 设置 额外参数
  setOptions(options: object);

  /**
   * 请求前置
   * 请求后置
   */
  requestBefore(callFunc: Function);
  requestAfter(callFunc: Function);

  /**
   * 设置缓存
   */
  cacheLocal(key: String);
  cacheSession(key: String);

  /**
   * 加密
   */
  encrypt();
  /**
   * 重写方法
   */
  // 重写错误
  resetNetError(error: TypeNetError);
  // 重写响应
  resetNetResponse(response: TypeNetResponse);

  // flag 请求
  flagGet(...arg: any[]): Promise<any>;
  flagPost(...arg: any[]): Promise<any>;
  flagPut(...arg: any[]): Promise<any>;
  flagPatch(...arg: any[]): Promise<any>;
  flagDel(...arg: any[]): Promise<any>;
  flagUpload(...arg: any[]): Promise<any>;
  flagUpImg(...arg: any[]): Promise<any>;
  // url 请求
  urlGet(...arg: any[]): Promise<any>;
  urlPost(...arg: any[]): Promise<any>;
  urlPut(...arg: any[]): Promise<any>;
  urlPatch(...arg: any[]): Promise<any>;
  urlDel(...arg: any[]): Promise<any>;
  urlUpload(...arg: any[]): Promise<any>;
  urlUpImg(...arg: any[]): Promise<any>;
}
export default RequestClass;
