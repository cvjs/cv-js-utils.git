

// const url = require('url');
// const u = require('underscore');

class WebpackAliyunOss {

  doWithWebpack(compiler) {
    var me = this;
    compiler.hooks.emit.tapAsync('WebpackAliyunOss', function (compilation, callback) {
      var publicPath = url.parse(compiler.options.output.publicPath);
      if (!publicPath.protocol || !publicPath.hostname) {
        return callback(new Error('Webpack配置文件中: "output.publicPath"必须设置为域名，例如： https://domain.com/path/'));
      }
      // var files = u.filter(u.keys(compilation.assets), me.options.filter);
      let assetsArr = Object.keys(compilation.assets);
      var files = assetsArr.filter(me.options.filter);

      if (files.length === 0) {
        return callback();
      }
      this.upload(files.shift(), me.options.retry).then(function () {
        console.log('[WebpackAliyunOss FINISHED]', 'All Completed');
        callback();
      }).catch(function (e) {
        console.log('[WebpackAliyunOss FAILED]', e);
        return callback(e);
      });
    });
  }

  upload(file, times) {
    var me = this;

    var target = url.resolve(url.format(publicPath), file);
    var key = url.parse(target).pathname;
    var source = compilation.assets[file].source();
    var body = Buffer.isBuffer(source) ? source : new Buffer(source, 'utf8');
    return me.client.put(key, body, {
      timeout: 30 * 1000
    }).then(function () {
      console.log('[WebpackAliyunOss SUCCESS]：', key);
      var next = files.shift();
      if (next) {
        return upload(next, me.options.retry);
      }
    }, function (e) {
      if (times === 0) {
        throw new Error('[WebpackAliyunOss ERROR]: ', e);
      }
      else {
        console.log('[WebpackAliyunOss retry]：', times, key);
        return upload(file, --times);
      }
    });
  }
}


module.exports = WebpackAliyunOss;

