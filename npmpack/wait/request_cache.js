class RequestCacheClass {
  // 默认的数据 - 存到缓存
  _initFormData(initData) {
    let pathOldStr = this.$route.path || this.$route.name;
    let pathNewStr = pathOldStr.replace(/\//g, '_');
    // let pathNewStr = pathOldStr.replaceAll('/', "_")
    console.log(pathOldStr, pathNewStr);
    sessionStorage.setItem(pathNewStr, JSON.stringify(initData));
  }
  // 验证存储 - 缓存数据
  _checkFormData(checkData) {
    let pathOldStr = this.$route.path || this.$route.name;
    let pathNewStr = pathOldStr.replace(/\//g, '_');
    let cacheData = sessionStorage.getItem(pathNewStr) || '';
    if (cacheData == '') {
      return true;
    }
    if (JSON.stringify(checkData) == cacheData) {
      console.log('表单没变化，允许离开');
      return false;
    } else {
      console.log('表单变化，询问是否保存');
      return true;
    }
  }

  // 表单数据处理 - 获取2个表单修改的字段
  _getFormChangeData(changeData) {
    let pathOldStr = this.$route.path || this.$route.name;
    let pathNewStr = pathOldStr.replace(/\//g, '_');
    let cacheData = sessionStorage.getItem(pathNewStr) || '';
    if (cacheData == '') {
      return changeData;
    }
    let oldData = JSON.parse(cacheData);

    let lastData = {};

    let aProps = Object.getOwnPropertyNames(oldData);
    let bProps = Object.getOwnPropertyNames(changeData);

    // console.log(aProps, bProps);
    //先判断两个数据length
    // if (aProps.length == bProps.length) {
    //循环拿到数值进行对比
    aProps.forEach((e) => {
      if (oldData[e] !== changeData[e]) {
        // console.log('数据改变')
        lastData[e] = changeData[e];
      } else {
        // console.log('数据不变')
        // return false;
      }
    });
    // } else { return false }
    return lastData;
  }
}
