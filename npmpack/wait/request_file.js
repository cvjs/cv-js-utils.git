import AxiosClass from 'axios';
import { Message, Loading } from 'element-plus';

function confirmEnding(str, target) {
  // 请把你的代码写在这里
  var start = str.length - target.length;
  var arr = str.substr(start, target.length);
  if (arr == target) {
    return true;
  }
  return false;
}
var httpFileObj = AxiosClass.create({
  baseURL: process.env.VUE_APP_LOCAL_BASE_DIR || '',
  timeout: 2000,
  headers: {},
  requests: {}
});
var loadingWin;
/**
 * 添加一个请求拦截器
 */
httpFileObj.interceptors.request.use(
  (config) => {
    loadingWin = Loading.service({
      text: '加载中',
      timeout: 300
    });
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);
/**
 * 添加一个响应拦截器
 */
httpFileObj.interceptors.response.use(
  function (response) {
    loadingWin.close();
    // console.log(response);
    /**
     * 加载 js 文件
     */
    // .indexOf('.js')
    // console.log(response);
    if (response.config && response.config.url && confirmEnding(response.config.url, '.js')) {
      console.warn('---', '加载 js 文件，解析js文件');
      var apiResultFunc = '';
      eval(' apiResultFunc = ' + response.data);
      let parseJsData = apiResultFunc();
      response.data = parseJsData;
    }
    if (response.config && response.config.url && confirmEnding(response.config.url, '.json')) {
      console.warn('---', '加载 json 文件');
    }
    // console.log(response, "返回参数1111")
    return response.data;
  },
  function (error) {
    loadingWin.close();
    console.warn('错误代码———————————————————————————', error);
    if (error == 'Error: Request failed with status code 404') {
      loadingWin.close();
    }
    var originalRequest = error.config;
    if (error.code == 'ECONNABORTED' && error.message.indexOf('timeout') != -1 && !originalRequest._retry) {
      originalRequest._retry = true;
      loadingWin.close();
      // return axios.request(originalRequest);
    }
    return Promise.reject(error);
  }
);
export default httpFile;

import HttpFileObj from '@/plugins/http_file';

try {
  // 获取 markdown 文件
  HttpFileObj.get(`/file/xxxx.md`, {})
    .then((succRes) => {})
    .catch(() => {});
  // 获取 js 文件
  HttpFileObj.get(`/file/xxxx.js`, {})
    .then((succRes) => {
      if (succRes.data) {
        let xxx = JSON.stringify(succRes.data, null, 2);
        let xx2 = JSON.stringify(succRes.data, null, 4);
      }
    })
    .catch(() => {});
} catch (error) {}
