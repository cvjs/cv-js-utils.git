import { defineConfig } from 'vite';
import { resolve } from 'path';

export default defineConfig({
  root: './example/',
  build: {
    outDir: '../npmpack/lib/',
    lib: {
      entry: '../src/main.js',
      name: 'cvUtils',
      fileName: 'cvUtils'
    },
    rollupOptions: {
      // 确保外部化处理那些你不想打包进库的依赖
      external: ['vue'],
      output: {
        // 在 UMD 构建模式下为这些外部化的依赖提供一个全局变量
        globals: {
          vue: 'Vue'
        }
      }
    }
    // minify: 'terser',
    // terserOptions: {
    //   //打包后移除console和注释
    //   compress: {
    //     drop_console: true,
    //     drop_debugger: true
    //   }
    // }
  },
  esbuild: {
    drop: ['console', 'debugger']
  }
});
