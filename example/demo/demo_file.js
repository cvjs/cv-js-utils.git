HttpObj.flagPost('SDK_FILE_COMMON_IMG_CONVERT', {
  convert_type: 'to_base64',
  convert_url: imgRemotePath
}).then((succRes) => {
  this.remoteImgBase64 = succRes.data ? succRes.data : '';
  let base64 = this.remoteImgBase64.split(',')[1];
  base64ToBlob({ b64data: base64, contentType: 'image/png' }).then((res) => {
    // 转后后的blob对象
    this.chuliHaibaoImg(key, res.preview);
  });
});
