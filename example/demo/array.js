// 正式导入请用
// import { arrayConcat, arrayLetterSort } from '@10yun/cv-js-utils';
// 以下为本地测试导入
import { arrayConcat, arrayLetterSort } from '../../src/main.js';

let arr1 = [
  { name: 'a', age: 10 },
  { name: 'c', age: 11 },
  { name: 'b', age: 13 }
];

let arr2 = [
  { name: 'f', age: 30 },
  { name: 'k', age: 21 },
  { name: 'v', age: 53 }
];
console.log(' arrayConcat ：', arrayConcat(arr1, arr2));

console.log(' arrayLetterSort ：', arrayLetterSort(arr1, 'name'));
