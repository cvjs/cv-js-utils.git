import { isStrZh, LibsDate } from '../../src/main.js';

/**
 * 判断
 */
console.log(` isStrZh('这个是否是中文') `, isStrZh('这个是否是中文'));
console.log(` isStrZh('asdcb') `, isStrZh('asdcb'));

/**
 * 日期测试
 */
// console.log(` LibsDate().format().value() `, LibsDate().format(' 季度 q').value());
// console.log(` LibsDate().init(1622974980).value() `, LibsDate().init(1622974980).value());
// console.log(` LibsDate().init(1622974980).value() `, LibsDate().init(1622974980).format('yyyy-MM-dd').value());
// console.log(` LibsDate().init("2021-06-06 18:23:00").value() `, LibsDate().init('2021-06-06 18:23:00').value('int'));
// console.log(` LibsDate().format('yyyy/MM/dd hh:mm:ss W').value() `, LibsDate().format('yyyy/MM/dd hh:mm:ss W').value());
// console.log(` LibsDate().format().value()  `, LibsDate().format('yyyy/MM/dd hh:mm:S').value());
// console.log(` LibsDate().format().value()  `, LibsDate().format('yyyy/03/15 hh:mm:S').value());
// console.log(` LibsDate().format('GMT').value()  `, LibsDate().format('GMT').value());
