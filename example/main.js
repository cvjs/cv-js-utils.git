import './style.css';
import javascriptLogo from './javascript.svg';
import { setupCounter } from '../src/main.js';
// import './demo/index2.js';
// import './demo/array.js';
import './demo/check.js';
// import './demo/file.js';

document.querySelector('#app').innerHTML = `
  <div>
    <a href="https://cvjs.cn/" target="_blank">
      <img src="./public/logo.jpg" class="logo" alt="Vite logo" />
    </a>
    <a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank">
      <img src="${javascriptLogo}" class="logo vanilla" alt="JavaScript logo" />
    </a>
    <h1>Hello CVJS!</h1>
    <div class="card">
      <button id="counter" type="button"></button>
    </div>
  </div>
`;
setupCounter(document.querySelector('#counter'));
