const fs = require('fs-extra');
const path = require('path');
const glob = require('glob');
const exec = require('child_process').exec;
const argv = process.argv.splice(2)[0] || '';

/* 版本号+1 */
const rootDir = path.resolve('.');

//第一个字段为发布项目根目录，可不传
const npmpackPath = path.join(rootDir, 'npmpack', argv);
const lib = path.join(npmpackPath, 'lib');

var packageData = fs.readFileSync(path.join(rootDir, 'package.json'), 'utf-8');
packageData = JSON.parse(packageData);
var libPackageData = fs.readFileSync(path.join(npmpackPath, 'package.json'), 'utf-8');
libPackageData = JSON.parse(libPackageData);
var currentVersion = '',
  currentVersionLib = '';
/* 自动升级版本号 */
versionUpdate(packageData, libPackageData);
/* 发布npm */
start();
/* 更新版本号 */
function versionUpdate(packageData, libPackageData) {
  currentVersion = packageData.version;
  currentVersionLib = libPackageData.version;
  if (packageData.version == libPackageData.version || packageData.name != libPackageData.name) {
    console.log('--- 版本号自动加1');
    let versionArr = libPackageData.version.split('.');
    versionArr[versionArr.length - 1]++;
    libPackageData.version = versionArr.join('.');
    packageData.version = versionArr.join('.');
  } else {
    console.log('--- 版本号已手动修改跳过自动自增');
    libPackageData.version = packageData.version;
  }
  /* 更新npm包版本 */
  fs.outputFileSync(path.join(npmpackPath, 'package.json'), JSON.stringify(libPackageData, '', 2));
  if (packageData.name == libPackageData.name) {
    //和根目录package的name一样 可以更新根目录版本号
    fs.outputFileSync(path.join(rootDir, 'package.json'), JSON.stringify(packageData, '', 2));
  }
}
/* 版本号回退 */
function versionBack() {
  console.log('发布失败，版本号回退至自增前');
  if (packageData.name == libPackageData.name) {
    //和根目录package的name一样 可以更新根目录版本号
    console.log(currentVersion, currentVersionLib);
    packageData.version = currentVersion;
    fs.outputFileSync('package.json', JSON.stringify(packageData, '', 2));
  } else {
    console.log(currentVersionLib);
  }
  libPackageData.version = currentVersionLib;
  fs.outputFileSync(path.join(npmpackPath, 'package.json'), JSON.stringify(libPackageData, '', 2));
}
function start() {
  // 任何你期望执行的cmd命令，ls都可以
  // npm config set registry https://registry.npmjs.org/
  let cmdStr1 = 'npm publish --access=public';
  //let cmdPath = path.join(__dirname, '..')

  //只需要传lib和同级文件
  let cmdPath = path.join(npmpackPath);
  let workerProcess = null;
  // 子进程名称

  runExec(cmdStr1, cmdPath, workerProcess);
}

function runExec(cmdStr, cmdPath, workerProcess) {
  workerProcess = exec(cmdStr, {
    cwd: cmdPath
  });
  // 打印正常的后台可执行程序输出
  workerProcess.stdout.on('data', function (data) {
    console.log(data);
  });
  // 打印错误的后台可执行程序输出
  workerProcess.stderr.on('data', function (data) {
    console.log(data);
  });
  // 退出之后的输出
  workerProcess.on('close', function (code) {
    console.log(!code ? '发布成功' : '发布失败：code' + code);
    if (code !== 0) {
      //版本号回退
      versionBack();
    }
  });
}
