/**
 * @title 拷贝
 */

/**
 * @action 数组深度克隆
 * @param {*} initalObj
 * @param {*} finalObj
 * @returns {Array}
 */
function objDeepClone(initalObj, finalObj) {
  var obj = finalObj || {};
  for (var i in initalObj) {
    // 避免相互引用对象导致死循环，如initalObj.a = initalObj的情况
    var prop = initalObj[i];
    if (prop === obj) {
      continue;
    }
    if (typeof prop === 'object') {
      obj[i] = prop.constructor === Array ? [] : Object.create(prop);
    } else {
      obj[i] = prop;
    }
  }
  return obj;
}
/**
 * @action 数组深度拷贝
 * @param {*} sourceOld
 * @returns {Array}
 */
function objDeepCopy(sourceOld) {
  let sourceCopy;
  // 深度复制数组
  if (Object.prototype.toString.call(sourceOld) == '[object Array]') {
    sourceCopy = [];
    for (let i in sourceOld) {
      sourceCopy.push(objDeepCopy(sourceOld[i]));
    }
    return sourceCopy;
  }
  // 深度复制对象
  if (Object.prototype.toString.call(sourceOld) == '[object Object]') {
    sourceCopy = {};
    for (let p in sourceOld) {
      sourceCopy[p] = sourceOld[p];
    }
    return sourceCopy;
  }
}

/**
 * @action 克隆对象
 * @param myObj
 * @returns {*}
 */
function cloneData(myObj) {
  if (typeof myObj !== 'object') return myObj;
  if (myObj === null) return myObj;
  //
  if (typeof myObj.length === 'number') {
    let [...myNewObj] = myObj;
    return myNewObj;
  } else {
    let { ...myNewObj } = myObj;
    return myNewObj;
  }
}

/**
 * @action 适用于任何数组和对象的深拷贝
 * @param obj
 * @returns {Mixid}
 *
 * 适用于任何数组和对象的深拷贝
 * const arr = [1, 2, 3, { a: "b" }, [{ c: "d" }]];
 * const cloneArr = cloneDeep(arr);
 *
 * cloneArr[3].a = "u";
 * cloneArr[4][0].c = "uu";
 *
 * console.log(arr); // [1, 2, 3, { a: "b" }, [{ c: "d" }]]
 * console.log(cloneArr); // [1, 2, 3, { a: "u" }, [{ c: "uu" }]]
 */
function cloneDeep(value) {
  if (typeof value !== 'object' || value === null) {
    return value;
  }
  let result;
  if (Array.isArray(value)) {
    result = [];
    for (let i = 0; i < value.length; i++) {
      result[i] = cloneDeep(value[i]);
    }
  } else if (value instanceof Date) {
    result = new Date(value.getTime());
  } else if (value instanceof RegExp) {
    result = new RegExp(value.source, value.flags);
  } else {
    // result = {} as any
    result = {};
    for (const key in value) {
      if (Object.prototype.hasOwnProperty.call(value, key)) {
        result[key] = cloneDeep(value[key]);
      }
    }
  }
  return result;
}
export { objDeepClone, objDeepCopy, cloneData, cloneDeep };
