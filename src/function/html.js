/**
 * @title html
 */

/**
 * @action html代码转义
 * @param sHtml
 * @returns {*}
 */
function html2Escape(sHtml) {
  if (!sHtml || sHtml == '') {
    return '';
  }
  return sHtml.replace(/[<>&"]/g, function (c) {
    return { '<': '&lt;', '>': '&gt;', '&': '&amp;', '"': '&quot;' }[c];
  });
}

export { html2Escape };
