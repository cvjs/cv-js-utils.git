/**
 * @title 字符串常用
 */

/**
 * @action 去掉字符串左右空格
 * @param {*} str
 * @returns {String}
 */
function strTrimSide(str = '') {
  if (!str) return '';
  return str.replace(/(^\s*)|(\s*$)/g, '');
}
/**
 * @action 去除字符串所有空格
 * @returns {String}
 */
function strTrimAll(str) {
  if (!str) return '';
  return str.replace(/\s|\xA0/g, '');
  // return result_str.replace(/\s/g, "");
  // (str + '').replace(/(\s+)$/g, '').replace(/^\s+/g, '');
}

/**
 * @action 获取文本长度
 * @param {*} str
 * @returns {Number}
 */
// 计算字符的长度 @param {String} str
function strLength(str) {
  if (typeof str === 'number' || typeof str === 'string') {
    return (str + '').length;
  }
  return 0;

  if (!str) {
    return 0;
  }
  // 返回字符串中全角字符的数组，然后用length计算出数目，如果没有匹配的则返回null，这个时候就使用空字符""，然后再加上字符串的length就是区分中英文的字符串的长度了
  var len = str.match(/[^\x00-\xff]/g); // 匹配Ascii码大于255字符
  return str.length + (len ? len.length : 0);
  // /<summary>获得字符串实际长度，中文2，英文1</summary>
  // /<param name="str">要获得长度的字符串</param>
  let realLength = 0;
  let strLen = str.length;
  let charCode = -1;
  for (var i = 0; i < strLen; i++) {
    charCode = str.charCodeAt(i);

    if (charCode > 255 || charCode < 0) {
      realLength += 2;
    } else {
      realLength++;
    }
    if (charCode >= 0 && charCode <= 128) {
      realLength += 1;
    } else {
      realLength += 2;
    }
  }
  return realLength;
}

/**
 * @action 判断字符是否是最小和最大之间
 * @param {*} val
 * @param {*} min
 * @param {*} max
 * @returns {Boolean}
 */
function strLenLimit(val, min, max) {
  var len = strLen(val);
  return !(len < min || len > max);
}
/**
 * @action 文本换行
 * @param {String} str
 * @returns {String}
 */
function strFeed(str) {
  if (!str) return '';
  return str.replace(/↵/g, '<br/>');
}

/**
 * @action 字符串是否包含
 * @param string
 * @param find
 * @param lower
 * @returns {Boolean}
 */
function strExists(string, find, lower = false) {
  string += '';
  find += '';
  if (lower !== true) {
    string = string.toLowerCase();
    find = find.toLowerCase();
  }
  return string.indexOf(find) !== -1;
}

/**
 * @action 字符串是否左边包含
 * @param string
 * @param find
 * @param lower
 * @returns {Boolean}
 */
function strLeftExists(string, find, lower = false) {
  string += '';
  find += '';
  if (lower !== true) {
    string = string.toLowerCase();
    find = find.toLowerCase();
  }
  return string.substring(0, find.length) === find;
}

/**
 * @action 删除左边字符串
 * @param string
 * @param find
 * @param lower
 * @returns {String}
 */
function strLeftDelete(string, find, lower = false) {
  string += '';
  find += '';
  if (strLeftExists(string, find, lower)) {
    string = string.substring(find.length);
  }
  return string ? string : '';
}
/**
 * @action 字符串是否右边包含
 * @param string
 * @param find
 * @param lower
 * @returns {Boolean}
 */
function strRightExists(string, find, lower = false) {
  string += '';
  find += '';
  if (lower !== true) {
    string = string.toLowerCase();
    find = find.toLowerCase();
  }
  return string.substring(string.length - find.length) === find;
}
/**
 * @action 删除右边字符串
 * @param string
 * @param find
 * @param lower
 * @returns {String}
 */
function strRightDelete(string, find, lower = false) {
  string += '';
  find += '';
  if (strRightExists(string, find, lower)) {
    string = string.substring(0, string.length - find.length);
  }
  return string ? string : '';
}

/**
 * @action 判断结尾
 * @param {*} str
 * @param {*} target
 * @returns {Boolean}
 */
function str_ends_with(str, target) {
  // 请把你的代码写在这里
  var start = str.length - target.length;
  var arr = str.substr(start, target.length);
  if (arr == target) {
    return true;
  }
  return false;
}

export {
  strTrimSide,
  strTrimAll,
  strLength,
  strFeed,
  strExists,
  strLeftExists,
  strLeftDelete,
  strRightExists,
  strRightDelete,
  str_ends_with
};
