/**
 * @title 生成随机
 */

// 生成uuid
function randomUuid2() {
  let s = [];
  let hexDigits = '0123456789abcdef';
  for (let i = 0; i < 36; i++) {
    s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
  }
  s[14] = '4'; // bits 12-15 of the time_hi_and_version field to 0010
  s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1); // bits 6-7 of the clock_seq_hi_and_reserved to 01
  s[8] = s[13] = s[18] = s[23] = '-';
  return s.join('');
}

/**
 * @action 生成随机UUID
 * @returns {String}
 */
function randomUUID() {
  let d = new Date().getTime();
  let uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    let r = (d + Math.random() * 16) % 16 | 0;
    d = Math.floor(d / 16);
    return (c == 'x' ? r : (r & 0x3) | 0x8).toString(16);
  });
  return uuid;
}
/**
 * @action 生成随机字符串 (大写、小写、数字)
 * @param {*} len
 * @returns {String}
 */
function randomString(len) {
  len = len || 32;
  // let charsStr = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
  let charsStr = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678oOLl9gqVvUuI1';
  /** **默认去掉了容易混淆的字符oOLl,9gq,Vv,Uu,I1*** */
  let maxPos = charsStr.length;
  let pwd = '';
  for (let i = 0; i < len; i++) {
    pwd += charsStr.charAt(Math.floor(Math.random() * maxPos));
  }
  return pwd;
}
/**
 * @action 生成随机字符串 (大写、小写、数字)
 * @param {*} randomFlag
 * @param {*} min
 * @param {*} max
 * @returns {String}
 */
function randomWord(randomFlag, min, max) {
  let str = '';
  let range = min;
  /* 数字 */
  let baseStr1 = '0,1,2,3,4,5,6,7,8,9';
  /* 小写字母 */
  let baseStr2 = 'a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z';
  /* 大写字母 */
  let baseStr3 = 'A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z';
  let arr = [];
  arr.concat(baseStr1.split(','));
  arr.concat(baseStr2.split(','));
  arr.concat(baseStr3.split(','));
  // 随机产生
  if (randomFlag) {
    range = Math.round(Math.random() * (max - min)) + min;
  }
  let timestamp = '';
  max = max || '';
  if (max == '') {
    timestamp = new Date().getTime();
  }
  let pos = '';
  for (let i = 0; i < range; i++) {
    pos = Math.round(Math.random() * (arr.length - 1));
    str += arr[pos];
  }
  return str + timestamp;
}
/**
 * @action 生成随机范围数字
 * 生成n-m的随机整数
 * @param Min
 * @param Max
 * @returns {Number}
 */
function randomNum(Min, Max) {
  let Range = Max - Min;
  let Rand = Math.random();
  //四舍五入
  return Min + Math.round(Rand * Range);
  return Math.ceil(Rand * Range, Min);
}

export { randomUUID, randomString, randomWord, randomNum };
