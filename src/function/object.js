/**
 * @title object对象
 */

/**
 * @action 排序object(JSON)对象
 * @param obj
 * @param ignore
 * @returns {Object}
 */
function objectSort(obj, ignore = []) {
  return Object.keys(obj)
    .sort()
    .reduce(function (result, key) {
      if (!ignore.includes(key)) {
        result[key] = obj[key];
      }
      return result;
    }, {});
}
/**
 * @action 判断两个对象是否相等
 * @param x
 * @param y
 * @returns {Boolean}
 */
function objectEquals(x, y) {
  let f1 = x instanceof Object;
  let f2 = y instanceof Object;
  if (!f1 || !f2) {
    return x === y;
  }
  if (Object.keys(x).length !== Object.keys(y).length) {
    return false;
  }
  for (let p in x) {
    if (x.hasOwnProperty(p)) {
      let a = x[p] instanceof Object;
      let b = y[p] instanceof Object;
      if (a && b) {
        if (!objectEquals(x[p], y[p])) {
          return false;
        }
      } else if (x[p] != y[p]) {
        return false;
      }
    }
  }
  return true;
}
export { objectSort, objectEquals };
