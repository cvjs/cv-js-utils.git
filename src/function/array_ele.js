/**
 *
 * @param initialData  =>  原始数据
 * @param config       =>  配置参数  （仅能接受收使用'configDef'里出现过的参数）
 * @param count        =>  当前层级
 * @param isSelfSub    =>  是否是自身子集
 * @return
 */
function cascaderDisabled(initialData, config, count = 1, isSelfSub) {
  let returnData = [];
  //整理配置
  const sett = Object.assign(
    {
      ownPrimary: '', //自身主键名
      ownPrimaryValue: '', //自身主键值
      level: 2, //限制层级
      children: 'tree' //自定义子集键名
    },
    config
  );

  initialData.forEach((router) => {
    const tmp = { ...router };
    if (count > sett.level || tmp[sett.ownPrimary] == sett.ownPrimaryValue || isSelfSub === true) {
      tmp.disabled = true;
    } else {
      tmp.disabled = false;
    }
    if (tmp[sett.children] && tmp[sett.children].length > 0) {
      tmp[sett.children] = cascaderDisabled(tmp[sett.children], config, count * 1 + 1, tmp.disabled);
    }

    returnData.push(tmp);
  });
  return returnData;
}
export { cascaderDisabled };
