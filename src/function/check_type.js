/**
 * @title 检测数据类型
 */

/**
 * @action 是否数组
 * @param {*} obj
 * @returns {Boolean}
 */
function isTypeArray(obj) {
  return Object.prototype.toString.call(obj) === '[object Array]';

  return (
    typeof obj == 'object' &&
    Object.prototype.toString.call(obj).toLowerCase() == '[object array]' &&
    typeof obj.length == 'number'
  );
}
/**
 * @action 是否布尔
 * @param {*} obj
 * @returns {Boolean}
 */
function isTypeBoolean(obj) {
  return Object.prototype.toString.call(obj) === '[object Boolean]';
}
/**
 * @action 是否日期
 * @param {*} obj
 * @returns {Boolean}
 */
function isTypeDate(obj) {
  return Object.prototype.toString.call(obj) === '[object Date]';
}
/**
 * @action 是否方法
 * @param {*} obj
 * @returns {Boolean}
 */
function isTypeFunction(obj) {
  return Object.prototype.toString.call(obj) === '[object Function]';
}
/**
 * @action 是否为空
 * @param {*} obj
 * @returns {Boolean}
 */
function isTypeNull(obj) {
  return Object.prototype.toString.call(obj) === '[object Null]';
}
/**
 * @action 是否数字
 * @param {*} obj
 * @returns {Boolean}
 */
function isTypeNumber(obj) {
  return Object.prototype.toString.call(obj) === '[object Number]';
}
/**
 * @action 是否对象
 * @param {*} obj
 * @returns {Boolean}
 */
function isTypeObj(obj) {
  return Object.prototype.toString.call(obj) === '[object Object]';
}
/**
 * @action 是否字符串
 * @param {*} obj
 * @returns {Boolean}
 */
function isTypeString(obj) {
  return Object.prototype.toString.call(obj) === '[object String]';
}
/**
 * @action 是否未定义
 * @param {*} obj
 * @returns {Boolean}
 */
function isTypeUndefined(obj) {
  return Object.prototype.toString.call(obj) === '[object Undefined]';
}
/**
 * @action 是否正则表达式
 * @param {*} obj
 * @returns {Boolean}
 */
function isTypeRegExp(obj) {
  return Object.prototype.toString.call(obj) === '[object RegExp]';
}
/**
 * @action 是否json数据
 * @param {*} obj
 * @returns {Boolean}
 */
function isTypeJsonString(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}
/**
 * @action 是否json数组对象
 * @param {*} obj
 * @returns {Boolean}
 */
function isTypeJson(obj) {
  return (
    typeof obj == 'object' &&
    Object.prototype.toString.call(obj).toLowerCase() == '[object object]' &&
    typeof obj.length == 'undefined'
  );
}
export {
  isTypeArray,
  isTypeBoolean,
  isTypeDate,
  isTypeFunction,
  isTypeNull,
  isTypeNumber,
  isTypeObj,
  isTypeRegExp,
  isTypeString,
  isTypeUndefined,
  isTypeJsonString,
  isTypeJson
};
