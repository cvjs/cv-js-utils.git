/**
 * @title 检测身份证
 */

/**
 * 验证身份证区域校验
 */
const _isIdCarcCheckCity = function (cardCode) {
  let cityStr = '11:北京,12:天津,13:河北,14:山西,15:内蒙古,';
  cityStr += '21:辽宁,22:吉林,23:黑龙江,';
  cityStr += '31:上海,32:江苏,33:浙江,34:安徽,35:福建,36:江西,37:山东,';
  cityStr += '41:河南,42:湖北,43:湖南,44:广东,45:广西,46:海南,';
  cityStr += '50:重庆,51:四川,52:贵州,53:云南,54:西藏,';
  cityStr += '61:陕西,62:甘肃,63:青海,64:宁夏,65:新疆,';
  cityStr += '71:台湾,81:香港,82:澳门,91:国外';
  let cityArr1 = cityStr.split(',');
  let cityArr2 = {};
  for (let cityIndex in cityArr1) {
    let cityItem = cityArr1[cityIndex].split(':');
    cityArr2[cityItem[0]] = cityItem[1];
  }
  return cityArr2[cardCode.substr(0, 2)];
};
const _isIdCarcCheckFormat = function (cardCode) {
  // let reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
  // let reg = /^[1-9]\d{5}(18|19|20)\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/;
  let reg = /^\d{6}(18|19|20)?\d{2}(0[1-9]|1[012])(0[1-9]|[12]\d|3[01])\d{3}(\d|X)$/i;
  return reg.test(cardCode);
};

/**
 * 验证是否是中国公民身份证号
 * @param {String}  cardCode 公民身份证号
 * @returns {Boolean}
 */
function _isIdCardCheck18(cardCode) {
  // 得到身份证数组
  let cardArr = cardCode.split('');
  // const factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2];
  // const parity = [1, 0, 'X', 9, 8, 7, 6, 5, 4, 3, 2];
  /**
   * ∑(ai×Wi)(mod 11)
   */
  // 加权因子 WI_ID_CODE
  const factor = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2, 1];
  // 校验位 VALIDE_ID_CODE
  const parity = [1, 0, 10, 9, 8, 7, 6, 5, 4, 3, 2];
  // 声明加权求和变量
  let sum = 0;
  if (cardArr[17].toLowerCase() == 'x') {
    cardArr[17] = 10; // 将最后位为x的验证码替换为10方便后续操作
  }
  for (let i = 0; i < 17; i++) {
    sum += cardArr[i] * factor[i]; // 加权求和
  }
  let valCodePosition = sum % 11; // 得到验证码所位置
  if (cardArr[17] == parity[valCodePosition]) {
    return true;
  } else {
    return false;
  }
}

/**
 * 判断18位身份证上面的出生日期是否正确
 */
function _isIdCardCheckBrithday(idCard) {
  let year = idCard.substring(6, 10);
  let month = idCard.substring(10, 12);
  let day = idCard.substring(12, 14);
  let temp_date = new Date(year, parseFloat(month) - 1, parseFloat(day));
  // 这里用getFullYear()获取年份，避免千年虫问题
  if (
    temp_date.getFullYear() != parseFloat(year) ||
    temp_date.getMonth() != parseFloat(month) - 1 ||
    temp_date.getDate() != parseFloat(day)
  ) {
    return false;
  } else {
    return true;
  }
}
/**
 * @action 判断是否是大陆身份证号码
 * 验证中国公民身份证号 - 对外
 * @param {String} cardCode
 * @returns {Boolean}
 */
function isIdCard(cardCode) {
  // 为空
  if (!cardCode) {
    return false;
  }
  // 去除字符串头尾空格
  cardCode = (cardCode + '').replace(/(^\s*)|(\s*$)/g, '');
  let len = cardCode.match(/[^\x00-\xff]/g); // 匹配Ascii码大于255字符
  let cardLen = cardCode.length + (len ? len.length : 0);
  if (cardLen != 18) {
    // 长度不满足
    return false;
  }
  if (
    // 格式验证
    _isIdCarcCheckFormat(cardCode) &&
    // 区域验证
    _isIdCarcCheckCity(cardCode) &&
    // 生日验证
    _isIdCardCheckBrithday(cardCode) &&
    // 第18位的验证
    _isIdCardCheck18(cardCode)
  ) {
    return true;
  }
  return false;
}
export { isIdCard };
