/**
 * @title 检测浏览器
 */

/**
 * @action 返回浏览器类型
 * @returns {String}
 */
function isBrowserType() {
  if (isBrowser_edge()) return 'edge';
  else if (isBrowser_opera()) return 'opera';
  else if (isBrowser_firefox()) return 'firefox';
  else if (isBrowser_weixin()) return 'weixin';
  else if (isBrowser_safari()) return 'safari';
  else if (isBrowser_chrome()) return 'chrome';
}
/**
 * @action 是否 IE
 * @returns {Boolean}
 */
function isBrowser_ie(UA) {
  const userAgent = UA || navigator.userAgent;
  const exactness =
    (userAgent.indexOf('MSIE') >= 0 && userAgent.indexOf('Opera') < 0) ||
    (userAgent.indexOf('MSIE') > -1 && userAgent.indexOf('compatible') > -1) ||
    (userAgent.indexOf('MSIE') > -1 && userAgent.indexOf('Trident') > -1);
  return exactness;
}
/**
 * @action 是否 IE11
 * @returns {Boolean}
 */
function isBrowser_ie11(UA) {
  const userAgent = UA || navigator.userAgent;
  const exactness = userAgent.indexOf('Trident') > -1 && userAgent.indexOf('rv:11.0') > -1;
  return exactness;
}
/**
 * @action 是否 Edge浏览器
 * @desc 是否IE的Edge浏览器
 * @returns {Boolean}
 */
function isBrowser_edge(UA) {
  const userAgent = UA || navigator.userAgent;
  const exactness = userAgent.indexOf('Edg') > -1;
  return exactness;
}
/**
 * @action 是否chrome浏览器
 * @returns {Boolean}
 */
function isBrowser_chrome(UA) {
  const userAgent = UA || navigator.userAgent;
  const exactness = userAgent.indexOf('Chrome') > -1;
  return exactness;

  // let ua = typeof window !== 'undefined' && window.navigator.userAgent.toLowerCase();
  // return ua.match(/Chrome/i) + '' === 'chrome';
}

/**
 * @action 是否opera浏览器
 * @returns {Boolean}
 */
function isBrowser_opera(UA) {
  const userAgent = UA || navigator.userAgent;
  const exactness = userAgent.indexOf('Opera') > -1 || userAgent.indexOf('OPR') > -1;
  return exactness;
}
/**
 * @action 是否Firefox浏览器
 * @returns {Boolean}
 */
function isBrowser_firefox(UA) {
  const userAgent = UA || navigator.userAgent;
  const exactness = userAgent.indexOf('Firefox') > -1;
  return exactness;
}
/**
 * @action 是否Safari浏览器
 * @returns {Boolean}
 */
function isBrowser_safari(UA) {
  const userAgent = UA || navigator.userAgent;
  const exactness =
    userAgent.indexOf('Safari') > -1 && userAgent.indexOf('Chrome') == -1 && userAgent.indexOf('MicroMessenger') == -1;
  return exactness;
}

/**
 * @action 是否苹果、chrome谷歌内核
 * @returns {Boolean}
 */
function isBrowser_webkit(UA) {
  const userAgent = UA || navigator.userAgent;
  const exactness = userAgent.indexOf('AppleWebKit') > -1;
  return exactness;
}
/**
 * @action 是否是微信
 * @desc 判断当前浏览器
 * @returns {Boolean}
 */
function isBrowser_weixin() {
  const userAgent = window.navigator.userAgent.toLowerCase();
  // 匹配ua中是否含有MicroMessenger字符串
  const exactness = userAgent.match(/MicroMessenger/i) == 'micromessenger' || userAgent.indexOf('MicroMessenger') > -1;
  return exactness;

  // let ua = typeof window !== 'undefined' && window.navigator.userAgent.toLowerCase();
  // return ua.match(/MicroMessenger/i) + '' === 'micromessenger';
}
/**
 * @action 是否qq
 * @param {*} UA
 * @returns {Boolean}
 */
function isBrowser_qq(UA) {
  const userAgent = UA || navigator.userAgent;
  const exactness = userAgent.match(/\sQQ/i) == ' QQ';
  return exactness;
  return /QQ/i.test(userAgent) ? true : false;
}

/**
 * @action 是否微博
 * @param {*} UA
 * @returns {Boolean}
 */
function isBrowser_weibo(UA) {
  const userAgent = UA || navigator.userAgent;
  return /Weibo/i.test(userAgent) ? true : false;
}

export {
  isBrowserType,
  isBrowser_ie,
  isBrowser_ie11,
  isBrowser_edge,
  isBrowser_chrome,
  isBrowser_opera,
  isBrowser_firefox,
  isBrowser_safari,
  isBrowser_webkit,
  isBrowser_weixin,
  isBrowser_qq,
  isBrowser_weibo
};
