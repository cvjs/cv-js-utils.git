import { isHave } from './check_common.js';
import { strExists } from './str_common.js';
/**
 * @title 字符串处理
 */

/**
 * @action 全局替换某个字符为另一个字符
 * @param {String} str 字符
 * @param {String} valueOne 包含的字符
 * @param {String} valueTwo 要替换的字符,选填
 * @returns {String}
 */
function strReplace(str, valueOne, valueTwo) {
  if (!str) return '';
  return str.replace(new RegExp(valueOne, 'g'), valueTwo);
}
/**
 * @action 金额千位分隔符
 * @param {Number|String} 金额
 * @returns {Number}
 */
function strFormatMoney(str) {
  var str = str.toString();
  return str.indexOf('.') != -1
    ? str.replace(/(\d)(?=(\d{3})+\.)/g, function ($0, $1) {
        return $1 + ',';
      })
    : str.replace(/(\d)(?=(\d{3}))/g, function ($0, $1) {
        return $1 + ',';
      });
}
/**
 * @action 字符每4位数一个间隔
 * @param {String} str 字符串
 */
function strFourSeparate(str) {
  if (!str) return '';
  return str
    .replace(/(\s)/g, '')
    .replace(/(\d{4})/g, '$1 ')
    .replace(/\s*$/, '');
}

/**
 * @action 隐藏银行卡号（ 仅显示后四位,其他*代替 ）
 * @param {String}  str 银行卡号
 * @returns {String}
 */
function strHideBankNo(str) {
  if (!str) return '';
  return str.replace(/\s/g, '').replace(/(\d{4})\d+(\d{4})$/, '**** **** **** $2');
}
/**
 * @action 隐藏手机号（ 仅显示前3后4位,其他*代替 ）
 * @param {String} str 手机号
 * @returns {String}
 */
function strHideMobile(str) {
  if (!str) return '';
  return str.replace(/^(\d{3})\d{4}(\d+)/, '$1****$2');
}
/**
 * @action 将字符串拆成字符，并存到数组中
 * @param {*} oldStr
 * @returns {String}
 */
function strSplitChars(oldStr) {
  var chars = new Array();
  for (var i = 0; i < oldStr.length; i++) {
    // chars[i] = [oldStr.substr(i, 1), this.isCHS(oldStr, i)];
  }
  oldStr.charsArray = chars;
  return chars;
}

/**
 * @action 过滤某字符串中的中文字符
 * @demo console.log( strFilterCN("我是js插件h-utils") ); // jsh-utils
 * @param {String} str
 * @returns {String}
 */
function strFilterCN(str = '') {
  return str.replace(/[\u4E00-\u9FA5]/g, '');
}

/**
 * @action 截取字符串（从start字节到end字节）
 * @param {*} oldStr
 * @param {*} start
 * @param {*} end
 * @returns {String}
 */
function strSubCHString(oldStr, start, end) {
  var len = 0;
  var str = '';
  oldStr = strSplitChars(oldStr);
  for (var i = 0; i < oldStr.length; i++) {
    if (oldStr.charsArray[i][1]) len += 2;
    else len++;
    if (end < len) return str;
    else if (start < len) str += oldStr.charsArray[i][0];
  }
  return str;
}
// 截取字符串（从start字节截取length个字节）
function strSubCHStr(oldStr, start, length) {
  return strSubCHString(oldStr, start, start + length);
}

/**
 * @action 截取字符串
 * @param string
 * @param start
 * @param end
 * @returns {String}
 */
function strSubString(string, start, end) {
  string += '';
  if (!isHave(end)) {
    end = string.length;
  }
  return string.substring(start, end);
}
// js截取字符串，中英文都能用
// 如果给定的字符串大于指定长度，截取指定长度返回，否者返回源字符串。
// 字符串，长度
/**
 * js截取字符串，中英文都能用
 * @param str：需要截取的字符串
 * @param len: 需要截取的长度
 * @returns {String}
 */
function strCutLen(str, len) {
  var str_length = 0;
  var str_len = 0;
  str_cut = new String();
  str_len = str.length;
  for (var i = 0; i < str_len; i++) {
    a = str.charAt(i);
    str_length++;
    if (encodeURI(a).length > 4) {
      // 中文字符的长度经编码之后大于4
      str_length++;
    }
    str_cut = str_cut.concat(a);
    if (str_length >= len) {
      str_cut = str_cut.concat('...');
      return str_cut;
    }
  }
  // 如果给定字符串小于指定长度，则返回源字符串；
  if (str_length < len) {
    return str;
  }
}
/**
 * @action 取字符串中间
 * @param string
 * @param start
 * @param end
 * @returns {Mixed}
 */
function strGetMiddle(string, start = null, end = null) {
  string = string.toString();
  if (isHave(start) && strExists(string, start)) {
    string = string.substring(string.indexOf(start) + start.length);
  }
  if (isHave(end) && strExists(string, end)) {
    string = string.substring(0, string.indexOf(end));
  }
  return string;
}

export {
  strReplace,
  strHideBankNo,
  strHideMobile,
  strFormatMoney,
  strFourSeparate,
  strSplitChars,
  strFilterCN,
  strSubString,
  strGetMiddle
};
