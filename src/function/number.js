/**
 * @title 数字
 */

/**
 *  @action 加法
 * @desc 加法函数 add，用来得到精确的加法结果
 * 说明：javascript的加法结果会有误差，在两个浮点数相加的时候会比较明显。这个函数返回较为精确的加法结果。
 * 调用：accAdd(arg1,arg2)
 * @param num 为保留位数,不传时不四舍五入
 * 返回值：arg1加上arg2的精确结果
 * @returns {Number}
 */
function numberAccAdd(arg1, arg2, num) {
  arg1 = arg1 || 0;
  arg2 = arg2 || 0;
  // arg1 = Number(arg1);
  // arg2 = Number(arg2);
  var r1, r2, m, c;
  try {
    r1 = arg1.toString().split('.')[1].length;
  } catch (e) {
    r1 = 0;
  }
  try {
    r2 = arg2.toString().split('.')[1].length;
  } catch (e) {
    r2 = 0;
  }
  c = Math.abs(r1 - r2);
  m = Math.pow(10, Math.max(r1, r2));
  if (c > 0) {
    var cm = Math.pow(10, c);
    if (r1 > r2) {
      arg1 = Number(arg1.toString().replace('.', ''));
      arg2 = Number(arg2.toString().replace('.', '')) * cm;
    } else {
      arg1 = Number(arg1.toString().replace('.', '')) * cm;
      arg2 = Number(arg2.toString().replace('.', ''));
    }
  } else {
    arg1 = Number(arg1.toString().replace('.', ''));
    arg2 = Number(arg2.toString().replace('.', ''));
  }

  if (num || num === 0) {
    // 注意：fixed四舍五入大于5会进1 5并不会进1
    return ((arg1 + arg2) / m).toFixed(num);
  }
  return (arg1 + arg2) / m;
}
/**
 * @action 减法
 * @desc 减法函数 reduce，用来得到精确的减法结果
 * 说明：javascript的减法结果会有误差，在两个浮点数相减的时候会比较明显。这个函数返回较为精确的减法结果。
 * 调用：accSub(arg1,arg2)
 * @param num 为保留位数,不传时不四舍五入
 * @returns {Number}
 */
function numberAccSub(arg1, arg2, num) {
  arg1 = arg1 || 0;
  arg2 = arg2 || 0;
  // arg1 = Number(arg1);
  // arg2 = Number(arg2);
  var r1, r2, m, n;
  try {
    r1 = arg1.toString().split('.')[1].length;
  } catch (e) {
    r1 = 0;
  }
  try {
    r2 = arg2.toString().split('.')[1].length;
  } catch (e) {
    r2 = 0;
  }
  m = Math.pow(10, Math.max(r1, r2)); //last modify by deeka //动态控制精度长度
  n = r1 >= r2 ? r1 : r2;

  if (num || num === 0) {
    //注意：fixed四舍五入大于5会进1 5并不会进1
    return ((arg1 * m - arg2 * m) / m).toFixed(num);
  }
  return ((arg1 * m - arg2 * m) / m).toFixed(n);
}
/**
 * @action 乘法
 * 乘法函数 multiply，用来得到精确的乘法结果
 * 说明：javascript的乘法结果会有误差，在两个浮点数相乘的时候会比较明显。这个函数返回较为精确的乘法结果。
 * 调用：accMul(arg1,arg2)
 * @param num 为保留位数,不传时不四舍五入
 * @returns {Number}
 */
function numberAccMul(arg1, arg2, num) {
  arg1 = arg1 || 0;
  arg2 = arg2 || 0;
  var m = 0,
    s1 = arg1.toString(),
    s2 = arg2.toString();
  try {
    m += s1.split('.')[1].length;
  } catch (e) {}
  try {
    m += s2.split('.')[1].length;
  } catch (e) {}

  if (num || num === 0) {
    //注意：fixed四舍五入大于5会进1 5并不会进1
    return ((Number(s1.replace('.', '')) * Number(s2.replace('.', ''))) / Math.pow(10, m)).toFixed(num);
  }
  return (Number(s1.replace('.', '')) * Number(s2.replace('.', ''))) / Math.pow(10, m);
}
/**
 * @action 除法
 * 除法函数 division，用来得到精确的除法结果
 * 说明：javascript的除法结果会有误差，在两个浮点数相除的时候会比较明显。这个函数返回较为精确的除法结果。
 * 调用：accDiv(arg1,arg2)
 * @param num 为保留位数,不传时不四舍五入
 * @returns {Number}
 */
function numberAccDiv(arg1, arg2, num) {
  var t1 = 0,
    t2 = 0,
    r1,
    r2;
  try {
    t1 = arg1.toString().split('.')[1].length;
  } catch (e) {}
  try {
    t2 = arg2.toString().split('.')[1].length;
  } catch (e) {}

  r1 = Number(arg1.toString().replace('.', ''));
  r2 = Number(arg2.toString().replace('.', ''));

  if (num || num === 0) {
    return ((r1 / r2) * Math.pow(10, t2 - t1)).toFixed(num);
  }
  return (r1 / r2) * Math.pow(10, t2 - t1);
}

/**
 * @action 四舍五入（保留小数点）
 * @param num{number}
 * @param n  {number}
 * @returns {Number}
 */
function numberRound(num, n) {
  n = n ? parseInt(n) : 0;
  if (n <= 0) {
    return Math.round(num);
  }
  num = Math.round(num * Math.pow(10, n)) / Math.pow(10, n); //四舍五入
  num = Number(num).toFixed(n); //补足位数
  return num;
}

export { numberAccAdd, numberAccSub, numberAccMul, numberAccDiv, numberRound };
