/**
 * @title array基础
 */

/**
 * @action 合并多个数组
 * @param {Array} arguments 支持N个数组传入
 * @returns {Array}
 */
function arrayConcat() {
  var arrNum = arguments.length || 0;
  var newArr = [];
  for (var i = 0; i < arrNum; i++) {
    newArr.push(...arguments[i]);
  }
  return newArr;
}
/**
 * @action 数组合并
 * @param target
 * @param source
 * @returns {Array}
 */
function arrayMerge(target, source) {}

/**
 * @action 数据对象拓展
 * @param target
 * @param source
 * @returns {Array}
 */
function arrayMixin(target, source) {
  var args = Array.prototype.slice.call(arguments);
  var i = 1,
    key,
    index;
  var ride = typeof args[args.length - 1] == 'boolean' ? args.pop() : true;
  if (args.length === 1) {
    target = this;
    i = 0;
  }
  while ((source = args[i++])) {
    for (key in source) {
      if (key in target) {
        if (typeof source[key] == 'object' && typeof target[key] == 'object') {
          target[key] = Object.assign(target[key], source[key]);
          // for (index in source[key]) {
          // target[key][index] = source[key][index];
          // }
        } else if (typeof source[key] == 'function' && typeof target[key] == 'function') {
          target[key] = source[key];
        }
      } else if (ride || !(key in target)) {
        target[key] = source[key];
      }
    }
  }
  return target;
}
function arrayExtend(target) {
  var sources = Array.prototype.slice.call(arguments, 1);
  for (var i = 0; i < sources.length; i += 1) {
    var source = sources[i];
    for (var key in source) {
      if (source.hasOwnProperty(key)) {
        target[key] = source[key];
      }
    }
  }
  return target;
}
/**
 * @action 获取数组最后一个值
 * @param array
 * @returns {Boolean,String}
 */
function arrayLast(array) {
  let str = false;
  if (typeof array === 'object' && array.length > 0) {
    str = array[array.length - 1];
  }
  return str;
}
/**
 * 数组排序
 * @param {Array} arr  数组
 * @param {Boolean} ascendFlag 升序,默认为 true
 * @returns {Array}
 */
function arraySort(arr, ascendFlag) {
  ascendFlag = typeof arguments[0] == 'undefined' ? true : arguments[0];
  return arr.sort((a, b) => {
    return ascendFlag ? a - b : b - a;
  });
}
/**
 * @action 数组根据英文字母排序
 * @param {Array}    arr  需要排序的数组 [{ name: 'a', age: 10 },{ name: 'c', age: 11 }],
 * @param {String}   sort_tag 排序的字段'name'
 * @param {Boolean}  isSaveNew 是否存放到新的数组里
 * @returns {Array}
 */
function arrayLetterSort(arr, sort_tag, isSaveNew) {
  var compare = function (prop) {
    return function (obj1, obj2) {
      var val1 = obj1[prop];
      var val2 = obj2[prop];
      if (val1 < val2) {
        return -1;
      } else if (val1 > val2) {
        return 1;
      } else {
        return 0;
      }
    };
  };
  var new_arr = arr.sort(compare(sort_tag));
  isSaveNew = isSaveNew || false;
  if (isSaveNew == true) {
    var new_arr2 = [];
    for (var i = 0; i < 26; i++) {
      // 输出A-Z 26个大写字母
      var letter_upper = String.fromCharCode(65 + i);
      // 输出a-z 26个小写字母
      var letter_lower = String.fromCharCode(97 + i);
      var letter_arr = [];
      for (let j in new_arr) {
        if (letter_upper === new_arr[j][sort_tag]) {
          if (new_arr[j] !== undefined && new_arr[j] !== null) {
            letter_arr.push(new_arr[j]);
          }
        }
      }
      if (letter_arr !== undefined && letter_arr.length > 0) {
        new_arr2.push({
          letter: letter_upper,
          tree: letter_arr
        });
      }
    }
    return new_arr2;
  } else {
    return new_arr;
  }
}
/**
 * @action 数组解析，格式转换
 * @param {Array}    needArr 需要解析的数组
 * @param {Object}   diyInitArr 需要转换自定义格式的配置 { 'formdiy_optval':'[]'}
 * @returns {Array}   重新处理过的数组
 */
function arrayParseType(needArr, diyInitArr) {
  var newArr = [];
  newArr = needArr;
  needArr.forEach(function (val, key) {
    for (var i in val) {
      if ('true' == val[i] || true == val[i]) {
        newArr[key][i] = true;
      }
      if ('false' == val[i] || false == val[i]) {
        newArr[key][i] = false;
      }
      for (var j in initArr) {
        if (i == j) {
          switch (initArr[j]) {
            case '[]':
              if ('' == val[i] || val[i] == undefined) {
                newArr[key][i] = [];
              }
          }
        }
      }
    }
  });
  return newArr;
}
/**
 * @action 判断是否包含在数组里
 * @param {*} arr
 * @param {*} search
 * @returns {Boolean}
 */
function arrayInText(arr, search) {
  for (s = 0; s < arr.length; s++) {
    thisEntry = arr[s].toString();
    if (thisEntry == search) {
      return true;
    }
  }
  return false;
}

/**
 * 是否在数组里
 * @param key
 * @param {Array} array
 * @param regular
 * @returns {Boolean|*}
 */
function arrayInArray(key, array, regular = false) {
  if (!Array.isArray(array)) {
    return false;
  }
  if (regular) {
    return !!array.find((item) => {
      if (item && item.indexOf('*')) {
        const rege = new RegExp('^' + item.replace(/[-\/\\^$+?.()|[\]{}]/g, '\\$&').replace(/\*/g, '.*') + '$', 'g');
        if (rege.test(key)) {
          return true;
        }
      }
      return item == key;
    });
  } else {
    return array.includes(key);
  }
}

/**
 * @action 数组是否包含某值
 * @param {Array} arr 数组
 * @param {*}  value 值,目前只支持 String,Number,Boolean
 * @returns {Array}
 */
function arrayInValue(arr, value) {
  return arr.includes(value);
}
/**
 * 数组中是否包含字符串
 * @param {*} arr
 * @param {*} str
 * @returns {Boolean}
 */
function arrayInStr(arr = [], str) {
  for (var f1 in arr) {
    if (arr[f1].indexOf(str) > -1) {
      return true;
    }
  }
  return false;
}

/**
 * @action 过滤数组，需要的字段
 * 过滤二维数组，将后端返回的对象进行重命名
 * @param {Array}    allArr 二维数组
 * @param {Object}   needField 需要的字段
 * @returns {Array}   新的数组
 */
function arrayFilterColumns(allArr, needField) {
  let newArray = [];
  if (typeof needField == 'string' && needField.constructor == String) {
    needField = needField.replace(/\s*/g, '');
    needField = needField.split(',');
  }
  allArr.forEach(function (arrVal, arrIndex) {
    newArray[arrIndex] = {};
    // needField.filter(function(key) {
    needField.map(function (key) {
      newArray[arrIndex][key] = ''; // 去null
      if (arrVal[key] !== undefined && arrVal[key] !== null) {
        newArray[arrIndex][key] = arrVal[key];
      }
    });
  });
  return newArray;
}
/**
 * @action 一维数组去重(字符串/数字)
 * @param {*} arr
 * @returns {Array}
 */
function arrayUnique(arr) {
  return Array.from(new Set(arr));
}
/**
 * @action 获取数组长度（处理数组不存在）
 * @param array
 * @returns {Number}
 */
function arrayLength(array) {
  if (array) {
    try {
      return array.length;
    } catch (e) {
      return 0;
    }
  }
  return 0;
}
export {
  arrayConcat,
  // arrayMerge,
  arrayMixin,
  arrayExtend,
  arrayLast,
  arrayLetterSort,
  arrayFilterColumns,
  arrayParseType,
  arrayInText,
  arrayInArray,
  arrayInValue,
  arrayUnique,
  arrayLength
};
