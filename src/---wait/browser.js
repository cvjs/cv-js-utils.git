function _uaMatch() {
  var ua = navigator.userAgent.toLowerCase(),
    match =
      /(opr)[ \/]([\w.]+)/.exec(ua) ||
      /(chrome)[ \/]([\w.]+)/.exec(ua) ||
      /(webkit)[ \/]([\w.]+)/.exec(ua) ||
      /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) ||
      /(msie) ([\w.]+)/.exec(ua) ||
      (ua.indexOf('compatible') < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua)) ||
      [];

  return {
    browser: match[1] || '',
    version: match[2] || '0'
  };
}
function init() {
  TNT.browser.modernizr();
  var matched = TNT.browser._uaMatch(),
    browser = {};
  if (matched.browser) {
    browser[matched.browser] = true;
    browser.version = matched.version;
  }
  // Chrome is Webkit, but Webkit is also Safari.
  if (browser.opr) {
    // opara >= 17 为 webkit
    browser.webkit = true;
    browser.opera = true;
  } else if (browser.chrome) {
    browser.webkit = true;
  } else if (browser.webkit) {
    browser.safari = true;
  }
  for (var key in browser) {
    TNT.browser[key] = browser[key];
  }
}
function browserType(browser) {
  switch (browser) {
    case 'ie':
      browser = 'msie';
      break;
    case 'firefox':
      browser = 'mozilla';
      break;
  }
  return !!TNT.browser[browser];
}
function modernizr(a, b, c) {
  var isSupport = (function () {
    var div = document.createElement('div'),
      vendors = 'Ms O Moz Webkit'.split(' ');
    return function (prop) {
      var len = vendors.length;
      if (prop in div.style) return true;
      prop = prop.replace(/^[a-z]/, function (val) {
        return val.toUpperCase();
      });
      while (len--) {
        if (vendors[len] + prop in div.style) {
          return true;
        }
      }
      return false;
    };
  })();
  var props = 'animation transition'.split(' '),
    l = props.length;
  while (l--) {
    if (!isSupport(props[l])) {
      $('html').addClass('no-' + props[l]);
    }
  }
}

export { _uaMatch, browserType, modernizr };
