/**
 * nodejs获取本地ip
 * @param os node.js os模块
 * @returns {*}
 */
function getIPAddress(os) {
  if (!os) return '';
  let interfaces = os.networkInterfaces();
  for (let devName in interfaces) {
    let iface = interfaces[devName];
    for (let i = 0; i < iface.length; i++) {
      let alias = iface[i];
      if (alias.family === 'IPv4' && alias.address !== '127.0.0.1' && !alias.internal) {
        return alias.address;
      }
    }
  }
}
