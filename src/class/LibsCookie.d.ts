declare class RequestClass {
  expires?: number;
  path?: string;
  domain?: string;
  setCookie(name: string, value: string, options?: any);
  delCookie(name: string, options?: any);
}
