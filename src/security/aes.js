import CryptoJS from 'crypto-js';
/**
 * AES算法类 算法模式: CBC
 * 密钥长度: 128位
 * 补码方式: PKCS5Padding（加解密）/ZeroPadding（加解密）
 * 密钥: ctocode-api
 * 密钥偏移量: api-token
 * 加密结果编码方式: base64字符串/十六进制字符串
 * 注意:需要引入 aes.js,pad-zeropadding-min.js,md5.js,base64.js
 *
 * @author: ctocode-zwj
 * @version: 1.0.0
 * @date: 2018/12/03
 */
function CTOCODE_Aes(keyStr, ivStr) {
  // 密钥
  this.keyStr = keyStr;
  // 密钥便宜量
  this.ivStr = ivStr;
  // 加密
  this.encrypt = function (data, code) {
    var keyStr = this.substrMd5(this.keyStr);
    var ivStr = this.substrMd5(this.ivStr);
    var key = CryptoJS.enc.Utf8.parse(keyStr);
    var iv = CryptoJS.enc.Utf8.parse(ivStr);
    var encrypt = CryptoJS.AES.encrypt(data, key, { iv: iv, mode: CryptoJS.mode.CBC });
    if (code == 'base64') {
      return encrypt.toString();
    } else if (code == 'hex') {
      var hexStr = encrypt.ciphertext.toString().toUpperCase();
      return CryptoJS.enc.Hex.parse(hexStr);
    } else {
      return false;
    }
    return encrypt;
  };
  // 解密
  this.decrypt = function (data, code) {
    var keyStr = this.substrMd5(this.keyStr);
    var ivStr = this.substrMd5(this.ivStr);
    var key = CryptoJS.enc.Utf8.parse(keyStr);
    var iv = CryptoJS.enc.Utf8.parse(ivStr);
    if (code == 'hex') {
      // 若果密文是hex，则需执行这个
      var encryptedHexStr = CryptoJS.enc.Hex.parse(data);
      var data = CryptoJS.enc.Base64.stringify(encryptedHexStr);
    }
    var decrypted = CryptoJS.AES.decrypt(data, key, { iv: iv, mode: CryptoJS.mode.CBC });
    return decrypted.toString(CryptoJS.enc.Utf8);
  };
  // md5后截取前16位数
  this.substrMd5 = function (str) {
    return hex_md5(str).substr(0, 16);
  };
}

/**
 * @action aes加密
 * @author ctocode-zwj
 * @version 2018-12-04
 */
function secAesEncrypt(str, code = 'base64', key = '10yun', iv = 'ctocodev3') {
  var _aes = new CTOCODE_Aes(key, iv);
  return _aes.encrypt(str, code);
}
/**
 * @action aes解密
 * @author ctocode-zwj
 * @version 2018-12-04
 */
function secAesDecrypt(str, code = 'base64', key = '10yun', iv = 'ctocodev3') {
  var _aes = new CTOCODE_Aes(key, iv);
  return _aes.decrypt(str, code);
}
