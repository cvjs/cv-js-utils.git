import { JSEncrypt } from 'jsencrypt';
/**
 * RSA算法类
 * 签名及密文编码：base64字符串/十六进制字符串/二进制字符串流
 * 填充方式: PKCS1Padding（加解密）/NOPadding（解密）
 *
 * Notice:Only accepts a single block. Block size is equal to the RSA key size!
 * 如密钥长度为1024 bit，则加密时数据需小于128字节，加上PKCS1Padding本身的11字节信息，所以明文需小于117字节
 * 注意：需引入base64.js,hex.js
 * js只能后端公钥加密，前端js私钥解密，js不能公钥解密
 *
 * @author: ctocode-zwj
 * @version: 1.0.0
 * @date: 2018/12/03
 */

function CTOCODE_RSA(publicKey, privateKey) {
  this.publicKey = publicKey;
  this.privateKey = privateKey;
  this.encrypt = new JSEncrypt();
  // private property
  this._keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';
  /**
   * @action 公钥加密
   * @param data 明文
   * @param 输出结果:base64/hex
   */
  this.publicEncrypt = function (data, code) {
    this.encrypt.setPublicKey(this.publicKey);
    var data = this.encrypt.encryptLong2(data, code); //返回结果已经base64
    return data;
  };
  /**
   * @action 私钥解密
   * @param data 密文
   * @param 输出结果:base64/hex
   */
  this.privateDecrypt = function (data, code) {
    this.encrypt.setPrivateKey(this.privateKey);
    return this.encrypt.decryptLong2(data, code);
  };
}

/**
 * @action rsa 加密
 * @author ctocode-zwj
 * @version 2018-12-04
 */
function secRsaEncrypt(str) {
  var _aes = new CTOCODE_Aes('10yun', 'ctocodev3');
  return _aes.encrypt(str, 'base64');
}
/**
 * @action rsa 解密
 * @author ctocode-zwj
 * @version 2018-12-04
 */
function secRsaDecrypt(str) {
  var _aes = new CTOCODE_Aes('10yun', 'ctocodev3');
  return _aes.decrypt(str, 'base64');
}
