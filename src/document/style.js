/**
 * 获取元素实际样式值
 * @param el dom元素
 * @param styleName 样式名
 */
function getStyle(el, styleName) {
  if (el.currentStyle) return el.currentStyle[styleName];
  if (getComputedStyle) return window.getComputedStyle(el, null)[styleName];
  return el.style[styleName];
}

/**
 * 获取元素实际高度
 * @param el  dom元素
 * @returns {Number} 元素高度
 */
function getHeight(el) {
  let height;
  // 已隐藏的元素
  if (getStyle(el, 'display') === 'none') {
    el.style.position = 'absolute';
    el.style.visibility = 'hidden';
    el.style.display = 'block';
    height = getStyle(el, 'height');
    el.style.position = '';
    el.style.visibility = '';
    el.style.display = '';
    return parseFloat(height);
  }
  return parseFloat(getStyle(el, 'height'));
}
