// 浏览器滚动到底部时执行fn函数
export function ScrollBottom(fn) {
  let $elem = document.documentElement;
  let $body = document.body;
  let scroll = $elem.scrollTop || $body.scrollTop; // 滚动条滚动的距离
  let clientH = $elem.clientHeight || $body.clientHeight; // 可视窗口总高度
  let scrollH = $elem.scrollHeight || $body.scrollHeight; // 窗口总高度
  let stayBottom = fn() || function () {};
  if (scroll >= scrollH - clientH) {
    stayBottom();
  }
}

export function scrollToTop() {
  let height = document.documentElement.scrollTop;
  clearInterval(window.timer);
  window.timer = null;
  let target = 0;
  window.timer = setInterval(function () {
    target = document.documentElement.scrollTop;
    target -= Math.ceil(target / 10); // 做减速运动
    window.scrollTo(0, target);
    if (target == 0) {
      clearInterval(timer);
      window.timer = null;
    }
  }, 10);
}
