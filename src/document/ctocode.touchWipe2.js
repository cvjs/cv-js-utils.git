/**
 * zepto插件：向左滑动删除动效 使用方法：$('.itemWipe').touchWipe({itemDelete: '.item-delete'}); 参数：itemDelete 删除按钮的样式名
 */
(function ($) {
  $.ctoTouchWipe = function (option) {
    var defaults = {
      width: '68', // 最大左滑动距离，默认80
      itemDelete: '.item-delete', // 删除元素
      bindEle: '',
      parentEle: ''
    };
    var opts = $.extend({}, defaults, option); // 配置选项

    //先作废，因 下来加载的时候，无该按钮元素，绑定无宽度
    //var delWidth = $(opts.itemDelete).width();
    var delWidth = opts.width; //根据当前页面设计，按钮宽度默认68

    var initX; // 触摸位置X
    var initY; // 触摸位置Y
    var moveX; // 滑动时的位置X
    var moveY; // 滑动时的位置Y
    var X = 0; // 移动距离X
    var Y = 0; // 移动距离Y
    var flagX = 0; // 是否是左右滑动 0为初始，1为左右，2为上下，在move中设置，在end中归零
    var objX = 0; // 目标对象位置

    if (opts.parentEle != '') {
      $(opts.parentEle).on('touchstart', opts.bindEle, function (event) {
        // console.log('start..');
        var obj = this;

        initX = event.originalEvent.targetTouches[0].pageX;
        initY = event.originalEvent.targetTouches[0].pageY;
        // console.log(initX + ':' + initY);
        objX = obj.style.WebkitTransform.replace(/translateX\(/g, '').replace(/px\)/g, '') * 1;
        // console.log(objX);
        if (objX == 0) {
          $(opts.parentEle).on('touchmove', opts.bindEle, function (event) {
            $(opts.parentEle).find(opts.bindEle).css('-webkit-transform', 'translateX(0px)');
            $(opts.itemDelete).on('click', function (e) {
              e.stopPropagation();
            });
            // 判断滑动方向，X轴阻止默认事件，Y轴跳出使用浏览器默认
            if (flagX == 0) {
              setScrollX(event);
              return;
            } else if (flagX == 1) {
              event.preventDefault();
            } else {
              return;
            }
            var obj = this;
            moveX = event.originalEvent.targetTouches[0].pageX;
            X = moveX - initX;
            if (X >= 0) {
              obj.style.WebkitTransform = 'translateX(' + 0 + 'px)';
            } else if (X < 0) {
              var l = Math.abs(X);
              obj.style.WebkitTransform = 'translateX(' + -l + 'px)';
              if (l > delWidth) {
                l = delWidth;
                obj.style.WebkitTransform = 'translateX(' + -l + 'px)';
              }
            }
          });
        } else if (objX < 0) {
          $(opts.parentEle).on('touchmove', opts.bindEle, function (event) {
            // 判断滑动方向，X轴阻止默认事件，Y轴跳出使用浏览器默认
            if (flagX == 0) {
              setScrollX(event);
              return;
            } else if (flagX == 1) {
              event.preventDefault();
            } else {
              return;
            }
            var obj = this;
            moveX = event.originalEvent.targetTouches[0].pageX;
            X = moveX - initX;
            if (X >= 0) {
              console.log(1);

              var r = -delWidth + Math.abs(X);
              obj.style.WebkitTransform = 'translateX(' + r + 'px)';
              if (r > 0) {
                r = 0;
                obj.style.WebkitTransform = 'translateX(' + r + 'px)';
              }
            } else {
              // 向左滑动
              obj.style.WebkitTransform = 'translateX(' + -delWidth + 'px)';
            }
          });
        }
      });
      // 结束时判断，并自动滑动到底或返回
      $(opts.parentEle).on('touchend', opts.bindEle, function (event) {
        var obj = this;
        objX = obj.style.WebkitTransform.replace(/translateX\(/g, '').replace(/px\)/g, '') * 1;
        if (objX > -delWidth / 2) {
          obj.style.transition = 'all 0.2s';
          obj.style.WebkitTransform = 'translateX(' + 0 + 'px)';
          obj.style.transition = 'all 0';
          objX = 0;
        } else {
          obj.style.transition = 'all 0.2s';
          obj.style.WebkitTransform = 'translateX(' + -delWidth + 'px)';
          obj.style.transition = 'all 0';
          objX = -delWidth;
        }
        flagX = 0;
      });
      // 链式返回
      return this;
    } else {
      $(opts.bindEle).on('touchstart', function (event) {
        // console.log('start..');
        var obj = this;
        initX = event.originalEvent.targetTouches[0].pageX;
        initY = event.originalEvent.targetTouches[0].pageY;
        // console.log(initX + ':' + initY);
        objX = obj.style.WebkitTransform.replace(/translateX\(/g, '').replace(/px\)/g, '') * 1;
        // console.log(objX);
        if (objX == 0) {
          $(opts.bindEle).on('touchmove', function (event) {
            // 判断滑动方向，X轴阻止默认事件，Y轴跳出使用浏览器默认
            if (flagX == 0) {
              setScrollX(event);
              return;
            } else if (flagX == 1) {
              event.preventDefault();
            } else {
              return;
            }
            var obj = this;
            moveX = event.originalEvent.targetTouches[0].pageX;
            X = moveX - initX;
            if (X >= 0) {
              obj.style.WebkitTransform = 'translateX(' + 0 + 'px)';
            } else if (X < 0) {
              var l = Math.abs(X);
              obj.style.WebkitTransform = 'translateX(' + -l + 'px)';
              if (l > delWidth) {
                l = delWidth;
                obj.style.WebkitTransform = 'translateX(' + -l + 'px)';
              }
            }
          });
        } else if (objX < 0) {
          $(opts.bindEle).on('touchmove', function (event) {
            // 判断滑动方向，X轴阻止默认事件，Y轴跳出使用浏览器默认
            if (flagX == 0) {
              setScrollX(event);
              return;
            } else if (flagX == 1) {
              event.preventDefault();
            } else {
              return;
            }
            var obj = this;
            moveX = event.originalEvent.targetTouches[0].pageX;
            X = moveX - initX;
            if (X >= 0) {
              var r = -delWidth + Math.abs(X);
              obj.style.WebkitTransform = 'translateX(' + r + 'px)';
              if (r > 0) {
                r = 0;
                obj.style.WebkitTransform = 'translateX(' + r + 'px)';
              }
            } else {
              // 向左滑动
              obj.style.WebkitTransform = 'translateX(' + -delWidth + 'px)';
            }
          });
        }
      });
      // 结束时判断，并自动滑动到底或返回
      $(opts.bindEle).on('touchend', function (event) {
        var obj = this;
        objX = obj.style.WebkitTransform.replace(/translateX\(/g, '').replace(/px\)/g, '') * 1;
        if (objX > -delWidth / 2) {
          obj.style.transition = 'all 0.2s';
          obj.style.WebkitTransform = 'translateX(' + 0 + 'px)';
          obj.style.transition = 'all 0';
          objX = 0;
        } else {
          obj.style.transition = 'all 0.2s';
          obj.style.WebkitTransform = 'translateX(' + -delWidth + 'px)';
          obj.style.transition = 'all 0';
          objX = -delWidth;
        }
        flagX = 0;
      });
      // 链式返回
      return opts.bindEle;
    }

    // 设置滑动方向
    function setScrollX(event) {
      moveX = event.originalEvent.targetTouches[0].pageX;
      moveY = event.originalEvent.targetTouches[0].pageY;
      X = moveX - initX;
      Y = moveY - initY;

      if (Math.abs(X) > Math.abs(Y)) {
        flagX = 1;
      } else {
        flagX = 2;
      }
      return flagX;
    }
  };
})($);

/* 监听滑动事件 */
function tabsTouch(touchFlag) {
  /* 单指拖动 */
  var touchObj = document.getElementById(touchFlag);
  var touchUlObj = $('#' + touchFlag);

  var Ul_width = touchUlObj.width();
  var win_w = $(window).width();
  var move_w = Ul_width - win_w;

  var currli = touchUlObj.find('li.on');
  if (currli.length != 0) {
    var currli_index = currli.index();
    var currli_left = currli.offset().left;
    var currli_w = currli.width();

    var center_w = currli_left + currli_w / 2 - win_w / 2;
    touchUlObj.find('li').each(function () {
      li_left = $(this).offset().left;
      li_w = $(this).width();
      if (li_left + li_w >= win_w) {
        m_index = $(this).index();
        if (currli_index >= m_index) {
          if (center_w > move_w) {
            touchUlObj.css({
              transform: 'translateX(' + -move_w + 'px)',
              '-webkit-transform': 'translateX(' + -move_w + 'px)',
              '-moz-transform': 'translateX(' + -move_w + 'px)'
            });
          } else {
            touchUlObj.css({
              transform: 'translateX(' + -(currli_left + currli_w / 2 - win_w / 2) + 'px)',
              '-webkit-transform': 'translateX(' + -(currli_left + currli_w / 2 - win_w / 2) + 'px)',
              '-moz-transform': 'translateX(' + -(currli_left + currli_w / 2 - win_w / 2) + 'px)'
            });
          }
        }
      }
    });
  }

  touchObj.addEventListener('touchstart', function (event) {
    var touch = event.targetTouches[0];
    var transZRegex = /\.*translateX\((.*)px\)/i;
    var style = touchObj.style;
    var transform = style.transform || style.WebkitTransform || style.MozTransform;
    var translateX = transZRegex.exec(transform)[1];
    var x = touch.pageX - translateX;

    touchObj.addEventListener(
      'touchmove',
      function (event) {
        // 如果这个元素的位置内只有一个手指的话
        if (event.targetTouches.length == 1) {
          event.preventDefault(); // 阻止浏览器默认事件，重要
          var touch = event.targetTouches[0];
          var all_move_len = touch.pageX - x;
          // 把元素放在手指所在的位置
          if (all_move_len <= -move_w) {
            all_move_len = -move_w;
          }
          if (all_move_len >= 0) {
            all_move_len = 0;
          }
          touchObj.style.transform = 'translateX(' + all_move_len + 'px)';
          touchObj.style.WebkitTransform = 'translateX(' + all_move_len + 'px)';
          touchObj.style.MozTransform = 'translateX(' + all_move_len + 'px)';
        }
      },
      false
    );
  });

  touchObj.addEventListener('touchend', function () {
    touchObj.removeEventListener('touchstart');
    touchObj.removeEventListener('touchmove');
  });
}
/* 监听滑动事件，并且调整li适应宽度 */
function tabsTouchInit(touchFlag) {
  var ulObj = $('#' + touchFlag);
  var _width = $(window).width();
  var liNum = ulObj.find('li').length;
  ulObj.find('li').each(function () {
    $(this).css({
      'min-width': _width / liNum
    });
    var has_em = $(this).find('span em').length;
    if (has_em == 0) {
      $(this).find('a').css({
        lineHeight: '30px'
      });
    }
  });
  window.addEventListener('load', tabsTouch(touchFlag), false);
}
/* 手指滑动tab end */
