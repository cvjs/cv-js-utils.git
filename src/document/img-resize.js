/* 自动 缩略图 img 和 p */
(function (win) {
  var $md_article = $('#goodsDetailContent');
  $(window).bind('load', function () {
    var newsImg = $md_article.find('img'),
      newsIframe = $md_article.find('iframe');
    resizeImgSize(newsImg);
    // resizeVideoSize(newsIframe);
  });
  var resizeImgSize = function (obj) {
    obj.each(function () {
      var self = $(this);
      if (self.attr('height')) {
        var maxWidth = $md_article.width() - 10,
          ratio = 0,
          width = self.width(),
          height = self.height();
        if (width > maxWidth) {
          ratio = maxWidth / width;
          self.css('width', maxWidth);
          self.css('height', height * ratio);
        }
      } else {
        var maxWidth = $md_article.width() - 10,
          ratio = 0,
          width = self.width(),
          height = self.height();
        if (width > maxWidth) {
          ratio = maxWidth / width;
          self.css('width', maxWidth);
          self.css('height', height * ratio);
        }
      }
    });
  };
  var resizeVideoSize = function (obj) {
    obj.each(function () {
      var _self = $(this);
      if (_self.attr('height')) {
        var maxWidth = $md_article.width() - 30,
          ratio = 9 / 16;
        _self.css('width', maxWidth);
        _self.css('height', maxWidth * ratio);
      }
    });
  };
})(window);
