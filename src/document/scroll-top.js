function testFunc() {
  /**
   * 方法1
   */
  window.scrollTo({
    top: gotoTop,
    behavior: 'smooth'
  });
  /**
   * 方法2
   */
  let distance = document.documentElement.scrollTop || document.body.scrollTop;
  console.log('---', gotoTop);
  scrollText(gotoTop);
  scrollToSmoothly(gotoTop, 10);

  /**
   * 方法3 ，因为顶部 60固定，所以要额外配置60
   * 但是好像无效
   * 用自带的方法  但是如果父亲和爷爷都有滚动条的话，都会动
   */
  anchorEle.scrollIntoView({
    behavior: 'smooth',
    block: 'start', // 将元素顶部与可见区域顶部对齐
    inline: 'nearest', // 在水平方向上选择最近的对齐方式
    // 因为顶部 60固定，所以要额外配置60
    top: anchorEle.getBoundingClientRect().top + 200 // 调整元素顶部位置
  });
}

/**
 * 自己写的滚动
 * @param gotoTop
 */
export function scrollText(gotoTop) {
  let currentPos = document.documentElement.scrollTop || document.body.scrollTop;
  console.log(currentPos, gotoTop, '-----');
  // 计算每一小段的距离
  if (currentPos < gotoTop) {
    /**
     * 向下
     */
    // let stepNum = gotoTop / 6;
    let stepTime = 15;
    let stepNum = 20;
    (function smoothDown1() {
      if (currentPos < gotoTop) {
        currentPos += stepNum;
        // 移动一小段
        // document.body.scrollTop = currentPos;
        document.documentElement.scrollTop = currentPos;
        // 设定每一次跳动的时间间隔为10ms
        setTimeout(smoothDown1, stepTime);
      } else {
        // 限制滚动停止时的距离
        // document.body.scrollTop = gotoTop;
        document.documentElement.scrollTop = gotoTop;
      }
    })();
  } else {
    /**
     * 向上
     */
    let stepTime = 10;
    let stepNum = 60;
    // let stepNum = gotoTop / 6;
    (function smoothDown2() {
      if (currentPos > gotoTop) {
        currentPos -= stepNum;
        // 移动一小段
        document.body.scrollTop = currentPos;
        document.documentElement.scrollTop = currentPos;
        // 设定每一次跳动的时间间隔为10ms
        setTimeout(smoothDown2, stepTime);
      } else {
        // 限制滚动停止时的距离
        document.body.scrollTop = gotoTop;
        document.documentElement.scrollTop = gotoTop;
      }
    })();
  }
}
/**
 * 这个好用
 * @param {*} gotoTop
 * @param {*} time
 */
export function scrollToSmoothly(gotoTop, time) {
  if (isNaN(gotoTop)) {
    throw 'Position must be a number';
  }
  if (gotoTop < 0) {
    throw 'Position can not be negative';
  }
  var currentPos = window.scrollY || window.screenTop;
  /**
   * 向下
   */
  if (currentPos < gotoTop) {
    let stepTime = 10;
    // let stepNum = 30;
    let stepNum = (gotoTop - currentPos) / 40;

    // let step = gotoTop / 6;
    for (let i = currentPos; i <= gotoTop; i += stepNum) {
      setTimeout(function () {
        window.scrollTo(0, i);
      }, stepTime / 2);
      stepTime += 10;
    }
  } else {
    /**
     * 向上
     */
    let stepTime = 10;
    let stepNum = (currentPos - gotoTop) / 20;
    (function smoothDown2() {
      if (currentPos > gotoTop) {
        currentPos -= stepNum;
        // 移动一小段
        document.body.scrollTop = currentPos;
        document.documentElement.scrollTop = currentPos;
        // 设定每一次跳动的时间间隔为10ms
        setTimeout(smoothDown2, stepTime);
      } else {
        // 限制滚动停止时的距离
        document.body.scrollTop = gotoTop;
        document.documentElement.scrollTop = gotoTop;
      }
    })();
  }
}
